#version 330 core
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

struct Material {
	sampler2D diffuse;
	sampler2D specular;
	sampler2D normal;
	float height;
	float shininess;
	bool useLight;
	bool useVertexColor;
	bool useSolidColor;
	vec4 solidColor;

	float reflectionPercentage;
};

struct DirLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	int filler;
};

struct PointLight {
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	int filler;
};

struct SpotLight {
	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	int filler;
};

#define NR_POINT_LIGHTS 4
#define NR_SPOT_LIGHTS 4
#define NR_DIR_LIGHTS 4

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec3 VertColour;


uniform vec3 threshold;
uniform vec3 viewPos;
uniform DirLight dirLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform SpotLight spotLights[NR_SPOT_LIGHTS];
uniform DirLight dirLights[NR_DIR_LIGHTS];
uniform Material material;
uniform vec3 defaultAmbient;

uniform samplerCube skybox;



uniform bool partymode;

// function prototypes
vec3
CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3
CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3
CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void
main()
{
	if(material.useSolidColor)
	{
		if(material.solidColor.a == -1)
		{
			FragColor = vec4(FragPos,1);
		}else
		{
			FragColor = material.solidColor;
		}
	}
	else{
		
		vec3 I = normalize(FragPos - viewPos);
		vec3 R = reflect(I, normalize(Normal));
    	FragColor =  texture(material.diffuse, TexCoords).rgba * (1- material.reflectionPercentage) + texture(skybox, R).rgba * material.reflectionPercentage;
	}

	if(partymode)
	{
		FragColor = vec4(FragPos, 1);
	}

	float brightness = dot(FragColor.xyz, vec3(0.2126, 0.7152, 0.0722));
    if(brightness < 0.255)
        BrightColor =  FragColor;
	else
		BrightColor = vec4(0,0,0,1);

}

// calculates the color when using a directional light.
vec3
CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);
	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec		= pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	// combine results
	vec3 ambient, diffuse;
	if (!material.useVertexColor) {
		 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
		 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
	}
    else {
		 ambient = light.ambient * VertColour;
		 diffuse = light.diffuse * diff * VertColour;
	}
	vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
	return (ambient + diffuse + specular);
}

// calculates the color when using a point light.
vec3
CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec		= pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation =
		1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	// combine results
	vec3 ambient, diffuse;
	if (!material.useVertexColor) {
		 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
		 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
	} else {
		 ambient = light.ambient * VertColour;
		 diffuse = light.diffuse * diff * VertColour;
	}

	vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;
	return (ambient + diffuse + specular);
}

// calculates the color when using a spot light.
vec3
CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);
	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);
	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec		= pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation =
		1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
	// spotlight intensity
	float theta		= dot(lightDir, normalize(-light.direction));
	float epsilon	= light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
	// combine results
	vec3 ambient;
	vec3 diffuse;
	if (!material.useVertexColor) {
		ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
		diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));

	} else {
		ambient = light.ambient * VertColour;
		diffuse = light.diffuse * diff * VertColour;
	}
	vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;
	return (ambient + diffuse + specular);
}
