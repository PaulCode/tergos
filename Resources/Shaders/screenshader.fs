#version 330 core

out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;
uniform sampler2D bloomTexture;


struct Vignette{
  	bool isUsed;
		float fallOff;
		float amount;
    float offset1;
    float offset2;
};

struct Blur {
  bool isUsed;
  float radius;
};

struct ColorCorrection {
bool isUsed;
vec3 colorCorr;
float exposure;
float contrast;
float saturation;
};


uniform bool hasBloom;
uniform bool hasToneMapping;
uniform int intensity;
uniform bool hasGammaCorrection;
uniform float gammaCorrection;
uniform vec2 screenSize;


uniform Blur blur;
uniform Vignette vignette;
uniform ColorCorrection colorCorr;

void blurFun(sampler2D image, vec2 uv, vec2 resolution, vec2 direction) {
  float Pi = 6.28318530718; // Pi*2
    
    // GAUSSIAN BLUR SETTINGS {{{
    float Directions = 6.0; // BLUR DIRECTIONS (Default 16.0 - More is better but slower)
    float Quality = 12.0; // BLUR QUALITY (Default 4.0 - More is better but slower)
    float Size = 16.0; // BLUR SIZE (Radius)
    // GAUSSIAN BLUR SETTINGS }}}
   
    vec2 Radius = Size/resolution.yx;
    
    // Normalized pixel coordinates (from 0 to 1)
      FragColor.rgb = pow(FragColor.rgb, vec3(1.0/gammaCorrection));
    // Pixel colour
    vec4 Color = FragColor;
    
    // Blur calculations
    for( float d=0.0; d<Pi; d+=Pi/Directions)
    {
      for(float i=1.0/Quality; i<=1.0; i+=1.0/Quality)
      {
        Color += texture( screenTexture, uv+vec2(cos(d),sin(d))*Radius*i);		
      }
    }
    
    // Output to screen
    Color /= Quality * Directions ;
    FragColor =  Color;
}


vec3 adjustContrast(vec3 color, float value) {
  return 0.5 + (1.0 + value) * (color - 0.5);
}

vec3 adjustExposure(vec3 color, float value) {
  return (1.0 + value) * color;
}

vec3 adjustSaturation(vec3 color, float value) {
  const vec3 luminosityFactor = vec3(0.2126, 0.7152, 0.0722);
  vec3 grayscale = vec3(dot(color, luminosityFactor));

  return mix(grayscale, color, 1.0 + value);
}

void main()
{
    FragColor = texture(screenTexture, TexCoords);

    float bloom = float(hasBloom);
    FragColor += texture(bloomTexture, TexCoords) * bloom;
    
    float isUsed = float(colorCorr.isUsed);
    
    FragColor = FragColor + vec4(colorCorr.colorCorr * isUsed, 1);
    FragColor.rgb = adjustContrast(FragColor.rgb, colorCorr.contrast * isUsed);
    FragColor.rgb = adjustExposure(FragColor.rgb, colorCorr.exposure * isUsed);
    FragColor.rgb = adjustSaturation(FragColor.rgb, colorCorr.saturation * isUsed);

    isUsed = float(hasGammaCorrection);

    float changeVal = 1.0/(gammaCorrection);

    if(hasGammaCorrection)FragColor.rgb = pow(FragColor.rgb, vec3(changeVal));

     isUsed = float(vignette.isUsed);
     float dist = distance(TexCoords, vec2(vignette.offset1, vignette.offset2));
     FragColor.rgb *= smoothstep(0.8, vignette.fallOff * 0.799, dist * (vignette.amount * isUsed + vignette.fallOff));
}
