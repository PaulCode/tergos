#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D image;

uniform vec2 screenRes;
uniform bool horizontal;


uniform float directions;
uniform float quality;
uniform float size;

void main()
{
    float pi2 = 6.28318530718; // Pi*2


    vec2 radius = size / screenRes.xy;

    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = TexCoords;

    // Pixel colour
    vec4 col = texture(image, uv).rgba;

    // Blur calculations
    for (float d = 0.0; d < pi2; d += pi2 / directions) {
        for (float i = 1.0 / quality; i <= 1.0; i += 1.0 / quality) {
            vec2 newUV = uv + vec2(cos(d), sin(d)) * radius * i;
            col = col + texture(image, newUV).rgba;
            
        }
    }

    // Output to screen
    col /= quality * directions - 15.0;
    FragColor = col;
}