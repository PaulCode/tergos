<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">Tergos</h3>

  <div align="center">
    Modern C++17 Rendering Engine
    <br />
    <a href="https://paulcode.gitlab.io/Tergos/"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="#quick-start-guide">View quick-start guide</a>
    ·
    <a href="https://gitlab.com/PaulCode/opengl/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=">Report Bug</a>
    ·
    <a href="https://gitlab.com/PaulCode/opengl/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=">Request Feature</a>
  </div>
</div>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#quick-start-guide">quick start guide</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

The goal of Tergos is to make it easy for developers to implement a 3D renderer in their projects, without having to learn complex graphics libraries like Vulkan, OpenGL or DirectX


<!-- GETTING STARTED -->
## Getting Started

You can get started right away by installing the prerequisites and following the commands in the [Installation](#installation) section.



### Prerequisites

- [**git**](https://git-scm.com/)
- [**cmake**](https://cmake.org/download/)
- **cpp+17 compiler**



### Installation
You can either build the library yourself as shown below or download the prebuild library:


<div align="center">
<a href="https://gitlab.com/PaulCode/opengl/-/jobs/artifacts/master/download?job=build-linux">Linux build</a>
<br>
<a href="https://gitlab.com/PaulCode/opengl/-/jobs/artifacts/master/download?job=build-windows-msvc16">Windows build</a>
</div>
<br>

The compiled Library is located in ```Build/lib/Engine.lib```


##### Linux

```shell
$ git clone --recursive https://gitlab.com/PaulCode/opengl && cd opengl
$ mkdir Build && cd Build
$ cmake ..
$ # Replace N with your core count
$ make -jN
```

##### Windows

```shell
$ git clone --recursive https://gitlab.com/PaulCode/opengl && cd opengl
$ mkdir Build && cd Build/
$ cmake -G "Visual Studio 16 2019" -T host=x64 -A x64 ..
$ # Debug, RelWithDebInfo or Release
$ # Replace N in /maxcpucount:N with your core count
$ cmake --build . --config Release --target ALL_BUILD -- /maxcpucount:N
```

##### Platform Independent

* Copy the Resources folder to the same directory the exe lies in. (Usually bin/Debug on Windows)
* If not cloned recursive use `git submodule update --init --recursive` to download the submodules 

<!-- USAGE EXAMPLES -->
## quick-start-guide

after successfully installing the library it can be included using 
```cpp  
#include <Engine.hpp>
```


A basic Setup that initializes the Engine and opens a Window:
```cpp

#include <Engine.hpp>
#include <iostream>

using namespace RenderEngine;
int main(void)
{                                            
	Engine* engine = Engine::GetInstance()->Init(nullptr, 640, 480, "Engine");
	
	while (!engine->ShouldClose())
	{
		engine->Update();
		engine->Render();
	}
	std::cout << "Program closed" << std::endl;
	return 1;
}

```
If you want to add a Model to the scene you add this infront of the loop:

```cpp

    RenderObject *obj = new RenderObject("object.obj");
    obj->SetName("firstObject"); // optional, but makes it easier to find the object later
    Engine::GetInstance()->AddObject(obj); 

```

If you want to have the project a bit better structured you can create a Logic.hpp file to handle updates and the initialisation of your scene:

```cpp
#include <Engine.hpp>
#include <glm/glm.hpp>
#include <iostream>
using namespace RenderEngine;
class NewLogic : public Logic
{
public:
  void Update()
  {
    // called every frame  on (engine->Update();)
  }
  void Init()
  {   
    // called during the Initialisation of the scene
  }
  NLogic() {}

  ~NLogic() {}

private:
};
```
At the location of your Engine Initialisation you now add the logic:
```cpp

	NewLogic* nLogic = new NewLogic();
	Engine* engine = Engine::GetInstance()->Init(nLogic, 640, 480, "Engine");

```

An example that includes a movable camera can be seen in [Tests/DemoTest](https://gitlab.com/PaulCode/opengl/-/tree/master/Tests/DemoTest).

A more in depth documentation, with explanation of every method and class, can be seen in [Documentation](https://paulcode.gitlab.io/opengl/index.html)



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/htldev/engine/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- MARKDOWN LINKS & IMAGES -->
[license-url]: https://gitlab.com/htldev/engine/blob/master/LICENSE
[product-preview]: Documents/preview.jpg
