#include "NLogic.hpp"
#include <Engine.hpp>
#include <iostream>

// Use this path if no one is given as an argument (useful for debugging)
#define MODEL_PATH ""

using namespace RenderEngine;
int
main(int argc, char *argv[])
{
	if (argc < 2 && strlen(MODEL_PATH) == 0) {
		std::cout << "Usage: " << argv[0] << " <model path>" << std::endl;
		return 1;
	}

	NLogic *logic		  = new NLogic();
	std::string modelpath = (argc >= 2) ? argv[1] : MODEL_PATH;
	logic->SetModelPath(modelpath);

	Engine *engine = Engine::GetInstance()->Init(logic, 960, 540, "Model Viewer");
	while (!engine->ShouldClose()) {
		engine->Update();
		engine->Render();
	}
	std::cout << "Program closed" << std::endl;
	return 0;
}
