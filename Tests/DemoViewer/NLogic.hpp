#include <Engine.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <chrono>
#include <memory>

#include "Graphics/imgui/imgui.h"

using namespace RenderEngine;

class NLogic : public Logic
{
public:
	void Update(float deltaTime) { UpdateCamera(); }

	void CreateUI()
	{
		if (showWindow) {
			ImGui::Begin("Options");
			ImGui::Checkbox("Enable Freecam", &freeCamMode);
			ImGui::End();
		}
	}

	void SetModelPath(std::string modelpath) { m_modelpath = modelpath; }

	void Init()
	{
		auto start				   = std::chrono::high_resolution_clock::now();
		auto obj				   = std::make_shared<RenderObject>(m_modelpath);
		float boundingSphereRadius = obj->GetBoundingSphereRadius();
		radius					   = boundingSphereRadius;
		cameraSpeed				   = boundingSphereRadius / 100;

		std::vector<std::string> skyboxPaths = {
			"Resources/Textures/skybox_venice/px.hdr", "Resources/Textures/skybox_venice/nx.hdr",
			"Resources/Textures/skybox_venice/py.hdr", "Resources/Textures/skybox_venice/ny.hdr",
			"Resources/Textures/skybox_venice/pz.hdr", "Resources/Textures/skybox_venice/nz.hdr"};

		Renderer::GetInstance()->GetSkyBox()->Setup(skyboxPaths);
		Renderer::GetInstance()->EnableSkyBox(true);
		Engine::GetInstance()->AddObject(obj);

		auto stop	  = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
		std::cout << "Loading took " << duration.count() << "ms" << std::endl;
	}
	NLogic() {}

	~NLogic() { std::cout << "Logic destroyed" << std::endl; }

private:
	float radius = 2;

	InstanceObject *instanceObj;
	std::string m_modelpath;
	float hRotAngle				  = 0;
	float vRotAngle				  = 0;
	bool freeCamMode			  = false;
	float cameraSpeed			  = 2.005f;
	const float mouseSpeed		  = 0.005f;
	double oldMousePosX			  = 0;
	double oldMousePosY			  = 0;
	bool oldLeftMouseClickedState = false;
	bool showWindow				  = true;

	void UpdateCamera()
	{
		Camera *camera			 = Engine::GetInstance()->GetCamera();
		Window *window			 = Window::GetInstance();
		GLFWwindow *windowHandle = window->GetWindow();

		float hCamAngle	   = camera->GetHorAngle();
		float vCamAngle	   = camera->GetVertAngle();
		glm::vec3 position = camera->GetPosition();

		if (glfwGetKey(windowHandle, GLFW_KEY_UP) == GLFW_PRESS) {
			radius *= 0.9;
		} else if (glfwGetKey(windowHandle, GLFW_KEY_DOWN) == GLFW_PRESS) {
			radius *= 1.1;
		}

		if (glfwGetMouseButton(windowHandle, GLFW_MOUSE_BUTTON_LEFT)) {
			double xpos, ypos;

			glfwGetCursorPos(windowHandle, &xpos, &ypos);

			double deltaX = xpos - oldMousePosX;
			double deltaY = ypos - oldMousePosY;

			if (oldLeftMouseClickedState) {
				if (freeCamMode) {
					hCamAngle += -deltaX * mouseSpeed;
					vCamAngle += -deltaY * mouseSpeed;
				} else {
					hRotAngle -= deltaX / 50;
					vRotAngle += deltaY / 50;
					vRotAngle  = std::clamp(vRotAngle, -glm::half_pi<float>(), glm::half_pi<float>());
					
					vCamAngle = -vRotAngle;
					hCamAngle = hRotAngle + glm::pi<float>();
				}
			}
			oldMousePosX			 = xpos;
			oldMousePosY			 = ypos;
			oldLeftMouseClickedState = true;
		} else {
			oldLeftMouseClickedState = false;
		}

		if (freeCamMode) {
			glm::vec3 direction(cos(vCamAngle) * sin(hCamAngle), sin(vCamAngle),
								cos(vCamAngle) * cos(hCamAngle));

			glm::vec3 right = glm::vec3(sin(hCamAngle - 3.14f / 2.0f), 0, cos(hCamAngle - 3.14f / 2.0f));

			if (glfwGetKey(windowHandle, GLFW_KEY_W) == GLFW_PRESS)
				position += direction * cameraSpeed;
			if (glfwGetKey(windowHandle, GLFW_KEY_S) == GLFW_PRESS) {
				position -= direction * cameraSpeed;
			}
			if (glfwGetKey(windowHandle, GLFW_KEY_A) == GLFW_PRESS)
				position -= right * cameraSpeed;
			if (glfwGetKey(windowHandle, GLFW_KEY_D) == GLFW_PRESS)
				position += right * cameraSpeed;
			if (glfwGetKey(windowHandle, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
				position.y -= cameraSpeed;
			if (glfwGetKey(windowHandle, GLFW_KEY_SPACE) == GLFW_PRESS)
				position.y += cameraSpeed;
		} else {
			position.x = sin(hRotAngle) * radius * cos(vRotAngle);
			position.y = sin(vRotAngle) * radius;
			position.z = cos(hRotAngle) * radius * cos(vRotAngle);
		}

		camera->SetPosition(position);
		camera->SetHorizontalAngle(hCamAngle);
		camera->SetVerticalAngle(vCamAngle);
	}
};
