#include "Base/Renderer.hpp"
#include "NLogic.hpp"
#include "BasicLogic.hpp"
#include <Engine.hpp>
#include <iostream>

using namespace RenderEngine;
int main( int argc, char *argv[])
{
	char y = NULL;
	if(argc > 1)  y = argv[1][0];
	Logic* nLogic = nullptr;

	switch (y)
	{
	case '0':
		nLogic = new BasicLogic();
		break;
	
	default:
			nLogic = new NLogic();
		break;
	}

	Engine* engine = Engine::GetInstance()->Init(nLogic, 960, 540, "Engine");
	Renderer::GetInstance()->SetupSkyBox({
		"Resources/Textures/showRoom/px.png", "Resources/Textures/showRoom/nx.png",
		"Resources/Textures/showRoom/py.png", "Resources/Textures/showRoom/ny.png",
		"Resources/Textures/showRoom/pz.png", "Resources/Textures/showRoom/nz.png"
    });
	Renderer::GetInstance()->EnableSkyBox(true);
	Renderer::GetInstance()->SetClearColour(0, 0, 0, 0);
	while (!engine->ShouldClose())
	{
		engine->Update();
		engine->Render();
	}
	std::cout << "Program closed" << std::endl;
	return 1;
}
