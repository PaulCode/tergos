
#include <Engine.hpp>
#include <glm/glm.hpp>
#include <iostream>

#include "Graphics/imgui/imgui.h"

using namespace RenderEngine;

class BasicLogic : public Logic
{
public:
	void Update(float deltaTime) { 
	}

	void CreateUI()
	{
	}

	void Init()
	{
	}

	BasicLogic() {}

	~BasicLogic() { std::cout << "Logic destroyed" << std::endl; }

private:
};
