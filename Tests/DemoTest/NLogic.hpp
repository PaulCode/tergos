#include <Engine.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <chrono>
#include <math.h>
#include "Base/Renderer.hpp"
#include "Graphics/Object/RenderObject.hpp"
#include "Graphics/imgui/imgui.h"
#include <memory>
#include <spdlog/spdlog.h>

#include "Graphics/Object/ParticleObjects/ParticleObject.hpp"
#include "Graphics/Object/ParticleObjects/ParticleGravityAffected.hpp"
#include "Graphics/Object/ParticleObjects/ExplodingParticle.hpp"
#include "glm/fwd.hpp"

using namespace RenderEngine;

class NLogic : public Logic
{
public:
	void Update(float deltaTime)
	{
		if(showHTL)
		{
		//		UpdateCamera();
		}
		else
		{
				if (cameraType == 0) {
				MoveCameraAround(deltaTime);
			} else if (cameraType == 1) {
				MoveCameraAroundNoYMovement(deltaTime);
			} else if (cameraType == 2) {
				UpdateCamera();
			}
			if (modelType == 0) {
				for(int i = 0; i < 9; i++)
				{
					ast->meshes[i].UseSolidColor(true);
				}
				Renderer::GetInstance()->EnableSkyBox(false);
				Renderer::GetInstance()->EnableLineRenderer(true);
				Renderer::GetInstance()->partyMode = true;

			} else if (modelType == 1) {
				for(int i = 0; i < 9; i++)
				{
					ast->meshes[i].UseSolidColor(true);
				}
				Renderer::GetInstance()->EnableLineRenderer(false);
				Renderer::GetInstance()->EnableSkyBox(false);
				Renderer::GetInstance()->partyMode = true;
			} else if (modelType == 2) {
				
				for(int i = 0; i < 9; i++)
				{
					ast->meshes[i].UseSolidColor(false);
				}
				
				Renderer::GetInstance()->EnableLineRenderer(false);
				Renderer::GetInstance()->EnableSkyBox(true);
				
				ast->meshes[0].GetMaterial()->reflectionPercentage = 0.0;
				ast->meshes[1].GetMaterial()->reflectionPercentage = 0.00;

				ast->meshes[4].GetMaterial()->reflectionPercentage = 0.0;

		//		ast->meshes[7].GetMaterial()->reflectionPercentage = 1;	



					ast->meshes[8].UseSolidColor(true);
					ast->meshes[8].SetSolidColor({1,1,1,1});
					// White light
					ast->meshes[7].UseSolidColor(true);
					ast->meshes[7].SetSolidColor({1,1,1,1});

					// Scaffolding
					ast->meshes[6].UseSolidColor(true);
					ast->meshes[6].SetSolidColor({0.1,0.1,0.1,1});

					// Floor
					ast->meshes[4].UseSolidColor(false);
					
					// Outside Wireframe
					ast->meshes[3].UseSolidColor(true);
					ast->meshes[3].SetSolidColor({0.1,0.1,0.1,1});

					// Outside Wireframe
					ast->meshes[3].UseSolidColor(true);
					ast->meshes[3].SetSolidColor({0.1,0.1,0.1,1});

					// Garage door
					ast->meshes[2].UseSolidColor(true);
					ast->meshes[2].SetSolidColor({0.2,0.2,0.2,1});
					
					Renderer::GetInstance()->partyMode = false;
			} else if (modelType == 3) {

				for(int i = 0; i < 9; i++)
				{
					ast->meshes[i].UseSolidColor(false);
				}

				Renderer::GetInstance()->EnableLineRenderer(false);
				Renderer::GetInstance()->EnableSkyBox(true);

				ast->meshes[0].GetMaterial()->reflectionPercentage = reflection;
				ast->meshes[1].GetMaterial()->reflectionPercentage = 0.00;

				ast->meshes[4].GetMaterial()->reflectionPercentage = 0.35;

				ast->meshes[7].GetMaterial()->reflectionPercentage = 1;	



					ast->meshes[8].UseSolidColor(true);
					ast->meshes[8].SetSolidColor({1,1,1,1});
					// White light
					ast->meshes[7].UseSolidColor(true);
					ast->meshes[7].SetSolidColor({1,1,1,1});

					// Scaffolding
					ast->meshes[6].UseSolidColor(true);
					ast->meshes[6].SetSolidColor({0.1,0.1,0.1,1});

					// Floor
					ast->meshes[4].UseSolidColor(false);
					
					// Outside Wireframe
					ast->meshes[3].UseSolidColor(true);
					ast->meshes[3].SetSolidColor({0.1,0.1,0.1,1});

					// Outside Wireframe
					ast->meshes[3].UseSolidColor(true);
					ast->meshes[3].SetSolidColor({0.1,0.1,0.1,1});

					// Garage door
					ast->meshes[2].UseSolidColor(true);
					ast->meshes[2].SetSolidColor({0.2,0.2,0.2,1});

					Renderer::GetInstance()->partyMode = false;
			}
		}
	}

	void CreateUI()
	{
		if (showWindow) {

			auto post = Engine::GetInstance()->GetPostProcessingPresets();

			Bloom bloom = post->GetBloom();
			ImGui::Checkbox("UseBloom", &bloom.useBloom);
			ImGui::DragFloat("Quality", &bloom.quality, 0.1,0,20);
			ImGui::DragFloat("Directions", &bloom.directions, 0.1,0,40);
			ImGui::DragFloat("Size", &bloom.size, 0.1,0,40);

			glm::vec3 tr = Renderer::GetInstance()->GetThreshold();

			ImGui::DragFloat("R threshold",&tr.x,0.01,0,1);
			ImGui::DragFloat("G threshold",&tr.y,0.01,0,1);
			ImGui::DragFloat("B threshold",&tr.z,0.01,0,1);

			Renderer::GetInstance()->SetThreshold(tr);

			post->SetBloom(bloom);

			ImGui::Checkbox("UseVignette", &post->GetVignette()->isUsed);
			ImGui::DragFloat("Radius",&post->GetVignette()->amount,0.1,0,10);
			ImGui::DragFloat("FallOff",&post->GetVignette()->fallOff,0.1,0,1);
			
			bool useGamma = post->GetHasGammaCorrection();
			ImGui::Checkbox("HasGamma", &useGamma);
			post->SetHasGammaCorrection(useGamma);
			float gamma = post->GetGammaCorrection();
			ImGui::DragFloat("GammaCorrection",&gamma,0.1,0,3);
			post->SetGammaCorrection(gamma);

			RenderEngine::ColorCorrection* colorCorr = post->GetColorCorr();
			ImGui::Checkbox("has Color Correction", &colorCorr->isUsed);

			ImGui::DragFloat("R",&colorCorr->colorCorr.x,0.01,-1,1);
			ImGui::DragFloat("G",&colorCorr->colorCorr.y,0.01,-1,1);
			ImGui::DragFloat("B",&colorCorr->colorCorr.z,0.01,-1,1);
			ImGui::DragFloat("Exposure",&colorCorr->exposure,0.01,-1,1);
			ImGui::DragFloat("Contrast",&colorCorr->contrast,0.01,-1,1);
			ImGui::DragFloat("Saturation",&colorCorr->saturation,0.01,-1,1);

			bool lightDebug = Renderer::GetInstance()->GetDebugLight();
			ImGui::Checkbox("Debug Light", &lightDebug);
			Renderer::GetInstance()->SetDebugLight(lightDebug);

			ImGui::Begin("Options");


			if(ImGui::Button("Switch Scene"))
			{
				showHTL = !showHTL;
				ast->SetVisibility(!showHTL);
				htl->SetVisibility(showHTL);

				if(showHTL)
				{
							std::vector<std::string> skyboxPaths = {
							"Resources/Textures/skybox_venice/px.hdr", "Resources/Textures/skybox_venice/nx.hdr",
							"Resources/Textures/skybox_venice/py.hdr", "Resources/Textures/skybox_venice/ny.hdr",
							"Resources/Textures/skybox_venice/pz.hdr", "Resources/Textures/skybox_venice/nz.hdr"};
								Renderer::GetInstance()->GetSkyBox()->Setup(skyboxPaths);
									Camera *cam = Engine::GetInstance()->GetCamera();

									cam->SetPosition({-0.002279892f ,0.44216147f, 1.3986793f});
									cam->SetHorizontalAngle(3.1650019f);
									cam->SetVerticalAngle(-0.09000002f);
							//	0.048758447 ,0.5819442 0.5819442 || 3.125001 || -0.100000024
				}else {
						Renderer::GetInstance()->SetupSkyBox({
							"Resources/Textures/showRoom/px.png", "Resources/Textures/showRoom/nx.png",
							"Resources/Textures/showRoom/py.png", "Resources/Textures/showRoom/ny.png",
							"Resources/Textures/showRoom/pz.png", "Resources/Textures/showRoom/nz.png"
						});
									Camera *cam = Engine::GetInstance()->GetCamera();
									cam->SetPosition({0, 1, 6});
									cam->SetHorizontalAngle(3.1650019f);
									cam->SetVerticalAngle(-0.09000002f);
				}
			}

			char* items[] = {"Wireframe", "No textures", "Textures", "Reflection"};

			if (ImGui::BeginCombo("Modeltype", current_item)) {
				for (int n = 0; n < IM_ARRAYSIZE(items); n++) {
					bool is_selected = (current_item == items[n]);
					if (ImGui::Selectable(items[n], is_selected)) {
						current_item = items[n];
						modelType = n;
					}
					
					if (is_selected) {
						ImGui::SetItemDefaultFocus(); 
					}
				}
				ImGui::EndCombo();
			}
			if (ImGui::Button("RL Movetype")) {
				cameraType = 0;
			}	
			if(ImGui::Button("Circle Movetype")) {
				cameraType = 1;
			}
			if(ImGui::Button("Freecam")) {
				cameraType = 2;
			}
			

			if(modelType ==3)
			{
				ImGui::DragFloat("Reflection", &reflection, 0.1,0,1);
			}

		//	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::End();
		}
	}

	void Init()
	{
		Camera *camera = Engine::GetInstance()->GetCamera();
		camera->SetPosition({0, 1, 6});
	//	ast = std::make_shared<RenderObject>("Resources/f1Car/gltf/f1_car.gltf")
		auto ast2 = std::make_shared<RenderObject>("Resources/f1Car/gltf/f1_2012_studio3.gltf");
		ast2->SetVisibility(false);
		ast = std::make_shared<RenderObject>("Resources/f1Car/gltf/f1_2012_studio3.gltf");
		htl = std::make_shared<RenderObject>("Resources/HTL/htl_testscene.gltf");

//		particleOb = std::make_shared<RenderObject>("Resources/ParticleObjects/untitled.gltf");

//		particles = std::make_shared<ParticleGravityAffected>(1000,particleOb,2,1,10,glm::vec3(0,0,-15));

	//	Engine::GetInstance()->particles.push_back(particles);

		ast->SetVisibility(false);

	


	//	ast->SetVisibility(false);


		
		

			Renderer::GetInstance()->EnableLineRenderer(false);
			Renderer::GetInstance()->EnableSkyBox(true);




			ast->meshes[0].GetMaterial()->reflectionPercentage = 0.35;
			ast->meshes[1].GetMaterial()->reflectionPercentage = 0.00;

			ast->meshes[4].GetMaterial()->reflectionPercentage = 0.35;

			ast->meshes[7].GetMaterial()->reflectionPercentage = 1;	



				ast->meshes[8].UseSolidColor(true);
				ast->meshes[8].SetSolidColor({1,1,1,1});
				// White light
				ast->meshes[7].UseSolidColor(true);
				ast->meshes[7].SetSolidColor({1,1,1,1});

				// Scaffolding
				ast->meshes[6].UseSolidColor(true);
				ast->meshes[6].SetSolidColor({0.1,0.1,0.1,1});

				// Floor
				ast->meshes[4].UseSolidColor(false);
				
				// Outside Wireframe
				ast->meshes[3].UseSolidColor(true);
				ast->meshes[3].SetSolidColor({0.1,0.1,0.1,1});

				// Outside Wireframe
				ast->meshes[3].UseSolidColor(true);
				ast->meshes[3].SetSolidColor({0.1,0.1,0.1,1});

				// Garage door
				ast->meshes[2].UseSolidColor(true);
				ast->meshes[2].SetSolidColor({0.2,0.2,0.2,1});


		Engine::GetInstance()->AddObject(htl);
		Engine::GetInstance()->AddObject(ast2);
		Engine::GetInstance()->AddObject(ast);
	}
	NLogic() {}

	~NLogic() { std::cout << "Logic destroyed" << std::endl; }

private:
Camera *camera = Engine::GetInstance()->GetCamera();
	char* current_item = NULL;
	std::shared_ptr<RenderObject> ast;
	std::shared_ptr<RenderObject> htl;


	std::shared_ptr<RenderObject> particleOb;

	std::shared_ptr<ParticleGravityAffected> particles;


	bool showHTL = true;

	std::string m_modelpath;
	float hRotAngle				  = 0;
	float vRotAngle				  = 0;
	bool freeCamMode			  = false;
	float cameraSpeed			  = 0.15f;
	const float mouseSpeed		  = 0.005f;
	double oldMousePosX			  = 0;
	double oldMousePosY			  = 0;
	bool oldLeftMouseClickedState = false;
	bool showWindow				  = true;


	bool useWireFrame = false;

	int modelType = 3;
	int cameraType;


	float radius =  0;
	float reflection = 0.35;
	float timer = 0;

	float cameraRadius = 2;

	void MoveCameraAroundNoYMovement(float deltaTime)
	{
		Camera *camera			 = Engine::GetInstance()->GetCamera();
		Window *window			 = Window::GetInstance();
		GLFWwindow *windowHandle = window->GetWindow();

		float hCamAngle	   = camera->GetHorAngle();
		float vCamAngle	   = camera->GetVertAngle();
		glm::vec3 position = camera->GetPosition();

		double xpos, ypos;

		double deltaX = 0.04;
		double deltaY = 0;

		hRotAngle -= deltaX / 50;
		vRotAngle += deltaY / 50;
		vRotAngle = std::clamp(vRotAngle, -glm::half_pi<float>(), glm::half_pi<float>());
		vCamAngle = -vRotAngle;
		hCamAngle = hRotAngle + glm::pi<float>();

		float rad  = 7;
		position.x = sin(hRotAngle) * rad * cos(vRotAngle);
		position.y = sin(vRotAngle) * rad + 1;
		position.z = cos(hRotAngle) * rad * cos(vRotAngle);

		camera->SetPosition(position);
		camera->SetHorizontalAngle(hCamAngle);
		camera->SetVerticalAngle(vCamAngle);
	}

	void MoveCameraAround(float deltaTime)
	{
		Camera *camera			 = Engine::GetInstance()->GetCamera();
		Window *window			 = Window::GetInstance();
		GLFWwindow *windowHandle = window->GetWindow();

		float hCamAngle	   = camera->GetHorAngle();
		float vCamAngle	   = camera->GetVertAngle();
		glm::vec3 position = camera->GetPosition();

		if (glfwGetKey(windowHandle, GLFW_KEY_UP) == GLFW_PRESS) {
			radius *= 0.9;
		} else if (glfwGetKey(windowHandle, GLFW_KEY_DOWN) == GLFW_PRESS) {
			radius *= 1.1;
		}
		double xpos, ypos;

		double deltaX = 0.04;
		double deltaY = 0.02;

		hRotAngle -= deltaX / 50;
		vRotAngle += deltaY / 50;
		vRotAngle = std::clamp(vRotAngle, -glm::half_pi<float>(), glm::half_pi<float>());
		vCamAngle = -vRotAngle;
		hCamAngle = hRotAngle + glm::pi<float>();

		float rad  = 6;
		position.x = sin(hRotAngle) * rad * cos(vRotAngle);
		position.y = sin(vRotAngle) * rad;
		position.z = cos(hRotAngle) * rad * cos(vRotAngle);

		camera->SetPosition(position);
		camera->SetHorizontalAngle(hCamAngle);
		camera->SetVerticalAngle(vCamAngle);
	}

	void UpdateCamera()
	{
		Camera *camera			 = Engine::GetInstance()->GetCamera();
		Camera *cam = Engine::GetInstance()->GetCamera();

		Window *window			 = Window::GetInstance();
		GLFWwindow *windowHandle = window->GetWindow();

		float horizontalAngle = camera->GetHorAngle();
		float verticalAngle	  = camera->GetVertAngle();
		glm::vec3 position	  = camera->GetPosition();

		if (glfwGetMouseButton(windowHandle, GLFW_MOUSE_BUTTON_LEFT)) {
			double xpos, ypos;

			glfwGetCursorPos(windowHandle, &xpos, &ypos);

			double deltaX = xpos - oldMousePosX;
			double deltaY = ypos - oldMousePosY;

			if (oldLeftMouseClickedState) {
				horizontalAngle += -deltaX * mouseSpeed;
				verticalAngle += -deltaY * mouseSpeed;
			}
			oldMousePosX			 = xpos;
			oldMousePosY			 = ypos;
			oldLeftMouseClickedState = true;
		} else {
			oldLeftMouseClickedState = false;
		}

		glm::vec3 direction(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle),
							cos(verticalAngle) * cos(horizontalAngle));

		glm::vec3 right =
			glm::vec3(sin(horizontalAngle - 3.14f / 2.0f), 0, cos(horizontalAngle - 3.14f / 2.0f));

		if (glfwGetKey(windowHandle, GLFW_KEY_W) == GLFW_PRESS)
			position += direction * cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_S) == GLFW_PRESS) {
			position -= direction * cameraSpeed;
		}
		if (glfwGetKey(windowHandle, GLFW_KEY_A) == GLFW_PRESS)
			position -= right * cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_D) == GLFW_PRESS)
			position += right * cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			position.y -= cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_SPACE) == GLFW_PRESS)
			position.y += cameraSpeed;

		camera->SetPosition(position);
		camera->SetHorizontalAngle(horizontalAngle);
		camera->SetVerticalAngle(verticalAngle);

	}
};
