#include "Base/Renderer.hpp"
#include "NLogic.hpp"
#include "BasicLogic.hpp"
#include <Engine.hpp>
#include <iostream>

using namespace RenderEngine;
int
main(int argc, char *argv[])
{
	char y = NULL;
	if (argc > 1)
		y = argv[1][0];
	Logic *nLogic = nullptr;

	switch (y) {
		case '0':
			nLogic = new BasicLogic();
			break;

		default:
			nLogic = new NLogic();
			break;
	}

	Engine *engine = Engine::GetInstance()->Init(nLogic, 960, 540, "Engine");
	Renderer::GetInstance()->SetClearColour(0.2, 0.2, 0.2, 1);
	while (!engine->ShouldClose()) {
		engine->Update();
		engine->Render();
	}
	std::cout << "Program closed" << std::endl;
	return 1;
}
