
#include <Engine.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <vector>
#include <spdlog/spdlog.h>

#include "Graphics/imgui/imgui.h"

using namespace RenderEngine;

class NLogic : public Logic
{
public:
	void Update(float deltaTime) { UpdateCamera(); }

	void CreateUI()
	{
		if (showWindow) {

			auto post = Engine::GetInstance()->GetPostProcessingPresets();

			Bloom bloom = post->GetBloom();
			ImGui::Checkbox("UseBloom", &bloom.useBloom);
			ImGui::DragFloat("Quality", &bloom.quality, 0.1, 0, 20);
			ImGui::DragFloat("Directions", &bloom.directions, 0.1, 0, 40);
			ImGui::DragFloat("Size", &bloom.size, 0.1, 0, 40);

			glm::vec3 tr = Renderer::GetInstance()->GetThreshold();

			ImGui::DragFloat("R threshold", &tr.x, 0.01, 0, 1);
			ImGui::DragFloat("G threshold", &tr.y, 0.01, 0, 1);
			ImGui::DragFloat("B threshold", &tr.z, 0.01, 0, 1);
			// ImGui::DragFloat("Threshold", &tr.x, 0.01, 0, 1);
			// tr.y = tr.x;
			// tr.z = tr.x;

			Renderer::GetInstance()->SetThreshold(tr);

			post->SetBloom(bloom);

			ImGui::Checkbox("UseVignette", &post->GetVignette()->isUsed);
			ImGui::DragFloat("Radius", &post->GetVignette()->amount, 0.1, 0, 10);
			ImGui::DragFloat("FallOff", &post->GetVignette()->fallOff, 0.1, 0, 1);

			bool useGamma = post->GetHasGammaCorrection();
			ImGui::Checkbox("HasGamma", &useGamma);
			post->SetHasGammaCorrection(useGamma);
			float gamma = post->GetGammaCorrection();
			ImGui::DragFloat("GammaCorrection", &gamma, 0.1, 0, 3);
			post->SetGammaCorrection(gamma);

			RenderEngine::ColorCorrection *colorCorr = post->GetColorCorr();
			ImGui::Checkbox("has Color Correction", &colorCorr->isUsed);

			auto exposure = &colorCorr->exposure;
			ImGui::DragFloat("R", &colorCorr->colorCorr.x, 0.01, -1, 1);
			ImGui::DragFloat("G", &colorCorr->colorCorr.y, 0.01, -1, 1);
			ImGui::DragFloat("B", &colorCorr->colorCorr.z, 0.01, -1, 1);
			// ImGui::DragFloat("Exposure", exposure, 0.01, -1, 1);
			ImGui::DragFloat("Contrast", &colorCorr->contrast, 0.01, -1, 1);
			ImGui::DragFloat("Saturation", &colorCorr->saturation, 0.01, -1, 1);

			if (bloom.useBloom) {
				colorCorr->exposure = -0.6;
			} else {
				colorCorr->exposure = 0;
			}
		}
	}

	void Init()
	{
		RenderObject *studio = new RenderObject("Resources/bloomdemo/htl_testscene3.gltf");

		// Green Ball
		spdlog::info("Mesh count: {}", studio->meshes.size());

		std::vector<glm::vec4> colors = {
			{0.023, 1.0, 0.918, 1}, {1, 0.08, 0.25, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1},
		};
		for (int i = 0; i < studio->meshes.size() - 1; i++) {
			studio->meshes.at(i).SetSolidColor(colors.at(i));
			studio->meshes.at(i).UseSolidColor(true);
		}

		Engine::GetInstance()->AddObject(studio);
	}

	NLogic() {}

	~NLogic() { std::cout << "Logic destroyed" << std::endl; }

private:
	float pos = 0;
	InstanceObject *instanceObj;
	float cameraSpeed			  = 0.050f;
	const float mouseSpeed		  = 0.005f;
	double oldMousePosX			  = 0;
	double oldMousePosY			  = 0;
	bool oldLeftMouseClickedState = false;
	bool showWindow				  = true;
	Texture *targetTexture;

	int counter = 0;
	void UpdateCamera()
	{
		Camera *camera = Engine::GetInstance()->GetCamera();

		counter++;

		Camera *cam = Engine::GetInstance()->GetCamera();

		Window *window			 = Window::GetInstance();
		GLFWwindow *windowHandle = window->GetWindow();

		float horizontalAngle = camera->GetHorAngle();
		float verticalAngle	  = camera->GetVertAngle();
		glm::vec3 position	  = camera->GetPosition();

		if (glfwGetMouseButton(windowHandle, GLFW_MOUSE_BUTTON_LEFT) && !ImGui::GetIO().WantCaptureMouse) {
			double xpos, ypos;

			glfwGetCursorPos(windowHandle, &xpos, &ypos);

			double deltaX = xpos - oldMousePosX;
			double deltaY = ypos - oldMousePosY;

			if (oldLeftMouseClickedState) {
				horizontalAngle += -deltaX * mouseSpeed;
				verticalAngle += -deltaY * mouseSpeed;
			}
			oldMousePosX			 = xpos;
			oldMousePosY			 = ypos;
			oldLeftMouseClickedState = true;
		} else {
			oldLeftMouseClickedState = false;
		}

		glm::vec3 direction(cos(verticalAngle) * sin(horizontalAngle), sin(verticalAngle),
							cos(verticalAngle) * cos(horizontalAngle));

		glm::vec3 right =
			glm::vec3(sin(horizontalAngle - 3.14f / 2.0f), 0, cos(horizontalAngle - 3.14f / 2.0f));

		if (glfwGetKey(windowHandle, GLFW_KEY_W) == GLFW_PRESS)
			position += direction * cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_S) == GLFW_PRESS) {
			position -= direction * cameraSpeed;
		}
		if (glfwGetKey(windowHandle, GLFW_KEY_A) == GLFW_PRESS)
			position -= right * cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_D) == GLFW_PRESS)
			position += right * cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
			position.y -= cameraSpeed;
		if (glfwGetKey(windowHandle, GLFW_KEY_SPACE) == GLFW_PRESS)
			position.y += cameraSpeed;

		camera->SetPosition(position);
		camera->SetHorizontalAngle(horizontalAngle);
		camera->SetVerticalAngle(verticalAngle);
	}
};
