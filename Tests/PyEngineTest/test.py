# For this to work, the path to pyEngine.py has to be
# set and _pyEngine.so (on Linux) has to be located
# in the same Directory as pyEngine.py

import sys
sys.path.append('<ENGINE_ROOT_DIR>/build/Bindings/python')

import pyEngine
e = pyEngine.Engine()
e.Init(None, 640, 480, "Engine Test")

while(not e.ShouldClose()):
    e.Update()
    e.Render()

print("Program Closed")