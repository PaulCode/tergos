#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Base/Camera.hpp>
#include <Graphics/Object/RenderObject.hpp>

namespace RenderEngine
{
    class Transformations
    {
    public:
        static glm::mat4 getModelMatrix(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale);
    };

} // namespace RenderEngine