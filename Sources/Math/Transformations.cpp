#include "Transformations.hpp"
#include "Base/Window.hpp"
#include <iostream>

namespace RenderEngine
{
    glm::mat4 Transformations::getModelMatrix(glm::vec3 pos, glm::vec3 rot, glm::vec3 scale)
    {
        glm::mat4 model = glm::mat4(1.0f);

        model = glm::translate(model, pos);

        model = glm::rotate(model, glm::radians(rot.x), glm::vec3(1, 0, 0));
        model = glm::rotate(model, glm::radians(rot.y), glm::vec3(0, 1, 0));
        model = glm::rotate(model, glm::radians(rot.z), glm::vec3(0, 0, 1));

        model = glm::scale(model, scale);

        return model;
    }

}