#pragma once

#include <Base/Renderer.hpp>

#include <Base/Camera.hpp>
#include <Base/Logic.hpp>
#include <Graphics/Light/Light.hpp>
#include <Graphics/Object/RenderObject.hpp>
#include <Graphics/ShaderProgram/ShaderProgram.hpp>
#include <Graphics/ShaderProgram/ShaderHandler.hpp>
#include <Graphics/PPP.hpp>
#include <Graphics/Object/ParticleObjects/ParticleObject.hpp>

#include <Graphics/Object/InstanceObject.hpp>
#include <Base/Window.hpp>
#include <Utils/TimeKeeper.hpp>
#include <Math/Transformations.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <corecrt_wstdio.h>
#include <glm/vec3.hpp>

#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
namespace RenderEngine
{

	/**
	 * @brief The base of the library.
	 *
	 */
	class Engine
	{
	public:
#pragma region EngineSpecific
		/**
		 * @brief Init the Engine here
		 *
		 * @param newlogic
		 * @param widht
		 * @param height
		 * @param name
		 */
		Engine *Init(Logic *newlogic, int widht, int height, const char *name);

		/**
		 * @brief can be called before or after Render();
		 *
		 */
		void Update();
		/**
		 * @brief used to Render the scene
		 *
		 */
		void Render();


		/**
		 * @brief 
		 * 
		 * @param camera 
		 * @param texture 
		 */
		void RenderToTexture(Camera* camera, Texture* texture, glm::vec2 textureSize = {-1,-1});
		/**
		 * @brief can limit the framerate if rendered continuously
		 *
		 * @param Limit
		 * @param FPSTime
		 */
		void LimitFrameRate(bool sync, int FPSTime = 60);
		/**
		 * @brief Get the Instance of the Engine
		 *
		 * @return Engine*
		 */
		static Engine *GetInstance();

		~Engine();


		/**
		 * @brief Check if the Window should close
		 *
		 * @return true
		 * @return false
		 */
		bool ShouldClose();
		/**
		 * @brief resets the engine to its original state
		 * 
		 */
		void ResetEngine();
#pragma endregion
#pragma region ObjectHandling
		/**
		 * @brief add new renderObjects
		 *
		 * @param object
		 * @return true
		 * @return false
		 */
		bool AddObject(std::shared_ptr<RenderObject> object);
		/**
		 * @brief remove RenderObjects
		 *
		 * @param object
		 * @return true
		 * @return false
		 */
		bool RemoveObject(std::shared_ptr<RenderObject>object);

		/**
		 * @brief add a instanced object to the render pipeline
		 *
		 * @param object
		 */
		void AddInstancedObject(std::shared_ptr<InstanceObject>object);

		/**
		 * @brief Get the list of all render objects
		 *
		 * @return std::vector<std::shared_ptr<RenderObject>>
		 */
		std::vector<std::shared_ptr<RenderObject>> GetAllObjects();

		/**
		 * @brief Get the the list of the instance Object
		 *
		 * @return std::vector<std::shared_ptr<InstanceObject>>*
		 */
		std::vector<std::shared_ptr<InstanceObject>> *GetAllInstanceObjects();

		/**
		 * @brief Gets a RenderObject by its name
		 *
		 * @param name
		 * @return std::shared_ptr<RenderObject>
		 */
		std::shared_ptr<RenderObject>GetObjectByName(std::string name);


		/**
		 * @brief allows to change the logic. Everything gets reset
		 * 
		 * @param logic 
		 */
		void SetNewLogic(Logic* logic);

#pragma endregion
#pragma region CameraHandling

		/**
		 * @brief Get the camera
		 *
		 * @return Camera*
		 */
		Camera *GetCamera();

		/**
		 * @brief sets the used camera
		 *
		 * @param camera
		 */
		void SetCamera(Camera camera);

#pragma endregion
#pragma region Light
		/**
		 * @brief Get the Light container, to add or remove ligts
		 *
		 * @return Light*
		 */
		Light *GetLight();

		/**
		 * @brief Set the Light container
		 *
		 * @param light
		 */
		void SetLight(Light *light);
#pragma endregion
#pragma region ShaderHandling

		/**
		 * @brief add a new ShaderHandler, Returns nullptr if error
		 *
		 * @param ShaderHandler
		 */
		std::shared_ptr<ShaderHandler> AddShaderHandler(ShaderHandler ShaderHandler);
		/**
		 * @brief Get a shaderHandler by its name
		 *
		 * @param name
		 * @return ShaderHandler
		 */
		std::shared_ptr<ShaderHandler> GetShaderHandlerByName(std::string name);

		PostProcessingPresets* GetPostProcessingPresets(){return &postProcessingPresets;}

#pragma endregion


	std::vector<std::shared_ptr<ParticleObject>> particles;
	std::vector<std::shared_ptr<InstanceObject>> instanceObjects;
	private:

		friend  class InstanceObject;
		
		static Engine *instance;
		Logic *logic;
		ShaderProgram instanceShader;
		Camera camera;
		Texture renderTarget;
		PostProcessingPresets postProcessingPresets;
		std::unordered_map<std::shared_ptr<ShaderHandler>, std::vector<std::shared_ptr<RenderObject>>> renderObjects;
		Light *m_Light = nullptr;

		float lastRenderTime = 0;
		Engine();
		// frameLimiter
		void Sync();
		bool limitFrame	   = false;
		double lastTime	   = -1;
		int targetFPS	   = 60;
		double timeCounter = 0;
		int frameCounter   = 0;
	};
} // namespace RenderEngine
