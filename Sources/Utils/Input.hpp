#pragma once

#include <tuple>
#include <Base/Window.hpp>

namespace RenderEngine
{

    /**
     * @brief a simple GLFW wrapper for input and output
     * 
     */
    class Input
    {
    public:
        static int GetKeyboardKeyStatus(int key);
        static int GetMouseButtonStatus(int button);
        static std::tuple<double, double> GetCursorPostion();
        enum MouseKeys { leftKey, rightKey, middleKey};
    };
}