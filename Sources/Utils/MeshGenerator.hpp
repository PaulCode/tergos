#include <Graphics/Object/Mesh.hpp>
namespace RenderEngine
{
    /**
     * @brief a collection of Functions that can generate simple meshes
     * 
     */
    class MeshGenerator
    {
    private:
    public:

    /**
     * @brief Generates a Plane
     * 
     * @param width 
     * @param length 
     * @return Mesh 
     */
    static Mesh Plane(int width, int length);
    /**
     * @brief Generates a Cuboid
     * 
     * @param width 
     * @param length 
     * @param height 
     * @return Mesh 
     */
    static Mesh Cuboid(int width,int length, int height);


    /**
     * @brief Generates a Triangle
     * 
     * @param width 
     * @param height 
     * @return Mesh 
     */
    static Mesh Triangle(int width, int height);
    };
    
}