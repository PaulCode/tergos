#include <GLFW/glfw3.h>
namespace RenderEngine
{

	/**
	 * @brief Used to track the time a render cylce takes. usefule for debugging
	 * 
	 */
	class TimeKeeper
	{
	private:
		double lastTime	   = -1;
		double timeCounter = 0;
		int frameCounter   = 0;
		int lastFPSTime = 0;
	public:
		TimeKeeper() { lastTime = glfwGetTime(); }
		/**
		 * @brief called every rendercycle
		 * 
		 */
		int Update()
		{
			double currenTime = glfwGetTime();
			frameCounter++;
			if (currenTime - lastTime >= 1.0) {
				frameCounter = 0;
				lastTime += 1.0;

				lastFPSTime =  1000.0 / double(frameCounter);
			}

			return lastFPSTime;
		}
	};

}