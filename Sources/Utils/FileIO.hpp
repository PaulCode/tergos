#pragma once

#include <string>
#include <iostream>
#include <optional>
#include <vector>
#include <unordered_map>
#include <GL/glew.h>

#include <Graphics/Object/Mesh.hpp>
#include <Graphics/Object/RenderObject.hpp>

#define STB_IMAGE_STATIC
#define STB_IMAGE_IMPLEMENTATION
#include "Image/stb_image.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace RenderEngine
{

    /**
     * @brief used to interact with files to load textures or models
     * 
     */
    class FileIO
    {
    public:

        struct Image {
            std::string type;
            std::string path;
            int width;
            int height;
            int nrComponents;
            unsigned char *data;
        };

        /**
         * @brief load a texture from a file. Uses the full path
         * 
         * @param path 
         * @param type 
         * @return Texture 
         */
        static Texture GetTexture(const char* path, std::string type);
        
        /**
         * @brief generate an OpenGL texture from raw pixeldata. The resulting OpenGL ID will be stored in the texture.
         * 
         * @param texture Texture to store the OpenGL texture id that gets generated from the imageFile
         * @param imageFile Contains the image dimensions and raw pixeldata
         */
        static void ApplyTexture(Texture &texture, Image imageFile);

        static std::vector<Mesh> loadModel(std::string path);


        static unsigned int LoadCubeMapTextures(std::vector<std::string> paths);
    private:
        static std::vector<Mesh> processNode(aiNode *node, const aiScene *scene,std::string directory);
        static std::optional<FileIO::Image> loadImageTexture(std::string directory, std::string path);
        static std::unordered_map<std::string, FileIO::Image> loadImageTextures(std::string directory, std::vector<std::string> paths);
		static Mesh processMesh(aiMesh *mesh, const aiScene *scene,std::string directory);
        static std::vector<std::string> getTexturePaths(aiMaterial *mat, aiTextureType type); 
    };
}
