#include "MeshGenerator.hpp"
#include <vector>

namespace RenderEngine
{
    Mesh MeshGenerator::Triangle(int width, int height){
		float halfWidth	 = width / 2.0f;
		float vertices[]={
			-halfWidth,	0,	 0.0f,
			halfWidth, 0, 0.0f,
			0,(float)height, 0.0f
		};
		float textureCoords[] = {1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};
		unsigned int indices[] = {0, 1, 2};

		Mesh mesh = Mesh(vertices, sizeof(vertices) / sizeof(float), indices,
						 sizeof(indices) / sizeof(unsigned int), textureCoords, nullptr);

		return mesh;
	}
	Mesh MeshGenerator::Plane(int width, int length)
	{
		float halfWidth	 = width / 2.0f;
		float halfLength = length / 2.0f;

		float vertices[] = {
			halfWidth,	halfLength,	 0.0f, // top right
			halfWidth,	-halfLength, 0.0f, // bottom right
			-halfWidth, -halfLength, 0.0f, // bottom left
			-halfWidth, halfLength,	 0.0f  // top left

		};
		float textureCoords[] = {1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};

		unsigned int indices[] = {0, 1, 3, 1, 2, 3};

		Mesh mesh = Mesh(vertices, sizeof(vertices) / sizeof(float), indices,
						 sizeof(indices) / sizeof(unsigned int), textureCoords, nullptr);

		return mesh;
	}

	Mesh MeshGenerator::Cuboid(int x,int y, int z)
	{
		float halfWidth	 = x / 2.0f;
		float halfHeight = y / 2.0f;
        float halfLength = z / 2.0f;

		float vertices[]= {
            halfWidth,halfLength ,halfHeight,
            halfWidth,-halfLength ,halfHeight,
            -halfWidth,-halfLength ,halfHeight,
            -halfWidth, halfLength , halfHeight,
            halfWidth,halfLength ,-halfHeight,
            halfWidth,-halfLength ,-halfHeight,
            -halfWidth,-halfLength ,-halfHeight,
            -halfWidth, halfLength ,-halfHeight

		 };
		float textureCoords[] = {
			0.0f,
			0.0f,
			0.0f,
			0.5f,
			0.5f,
			0.5f,
			0.5f,
			0.0f,

			0.0f,
			0.0f,
			0.5f,
			0.0f,
			0.0f,
			0.5f,
			0.5f,
			0.5f,

			// For text coords in top face
			0.0f,
			0.5f,
			0.5f,
			0.5f,
			0.0f,
			1.0f,
			0.5f,
			1.0f,

			// For text coords in right face
			0.0f,
			0.0f,
			0.0f,
			0.5f,

			// For text coords in left face
			0.5f,
			0.0f,
			0.5f,
			0.5f,

			// For text coords in bottom face
			0.5f,
			0.0f,
			1.0f,
			0.0f,
			0.5f,
			0.5f,
			1.0f,
			0.5f,
		};

		unsigned int indices[] = {0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 2, 3, 6, 3, 6, 7,
								  0, 1, 4, 1, 4, 5, 0, 3, 4, 3, 4, 7, 1, 2, 5, 1, 5, 6};

		Mesh mesh = Mesh(vertices, sizeof(vertices) / sizeof(float), indices,
						 sizeof(indices) / sizeof(unsigned int), textureCoords, nullptr);

		return mesh;
	}

}