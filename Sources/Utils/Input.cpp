#include "Input.hpp"

namespace RenderEngine
{
    int Input::GetKeyboardKeyStatus(int key)
    {
        return glfwGetKey(Window::GetInstance()->GetWindow(), key);
    }
    
    int Input::GetMouseButtonStatus(int button)
    {
        return glfwGetMouseButton(Window::GetInstance()->GetWindow(), button);
    }

    std::tuple<double, double> GetCursorPostion()
    {
        double xpos, ypos;
        glfwGetCursorPos(Window::GetInstance()->GetWindow(), &xpos, &ypos);
        return {xpos, ypos};
    }
}