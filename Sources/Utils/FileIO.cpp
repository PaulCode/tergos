#include "FileIO.hpp"
#include <set>
#include <mutex>
#include <future>
#include <filesystem>
#include <spdlog/spdlog.h>
namespace RenderEngine
{
#pragma region ObjectLoading

	std::vector<Mesh> FileIO::loadModel(std::string path)
	{
		std::vector<Mesh> meshes;
		Assimp::Importer importer;

		if (!std::filesystem::exists(path)) {
			spdlog::error("File \"{0}\" not found", path);
			return meshes;
		}

		std::string fileExtension = std::filesystem::path(path).extension().string();
		if (!importer.IsExtensionSupported(fileExtension)) {
			spdlog::error("{0} files are not supported by this version of assimp",fileExtension);
			return meshes;
		}

		const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals |
														   aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
		// check for errors
		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
		{
			spdlog::error( "ERROR::ASSIMP::{0}",importer.GetErrorString());
			return meshes;
		}
		// retrieve the directory path of the filepath
		// TODO: bug?
		std::string directory = std::filesystem::path(path).parent_path().string();

		// process ASSIMP's root node recursively
		return processNode(scene->mRootNode, scene, directory);
	}


	Texture FileIO::GetTexture(const char* path, std::string type){
		Texture texture;
		FileIO::Image image;

		std::optional<FileIO::Image> result = {};
		int w, h, comp;
		std::string filepath =   path;
		unsigned char *data = stbi_load(filepath.c_str(), &w, &h, &comp, 0);

		if (data) {
			image.data		   = data;
			image.height	   = h;
			image.width		   = w;
			image.nrComponents = comp;
			result			   = image;
		} else {
			spdlog::error("Could not load {0}",path);
		}

		ApplyTexture(texture,image);

		return texture;
	}



	std::vector<Mesh> FileIO::processNode(aiNode *node, const aiScene *scene, std::string directory)
	{
		std::vector<Mesh> meshes;
		meshes.reserve(node->mNumMeshes);

		std::set<std::string> imageFilenameSet;

		for (unsigned int i = 0; i < node->mNumMeshes; i++) {
			aiMesh *mesh	   = scene->mMeshes[node->mMeshes[i]];
			Mesh processedMesh = processMesh(mesh, scene, directory);

			// Diffuse
			std::vector<std::string> diffusePaths =
				getTexturePaths(scene->mMaterials[mesh->mMaterialIndex], aiTextureType_DIFFUSE);
			imageFilenameSet.insert(diffusePaths.begin(), diffusePaths.end());
			for (auto &&path : diffusePaths) {
				Texture tex = {0, "material.diffuse", path};
				processedMesh.AddTexture(tex);
			}

			// Normals
			std::vector<std::string> normalPaths =
				getTexturePaths(scene->mMaterials[mesh->mMaterialIndex], aiTextureType_NORMALS);
			imageFilenameSet.insert(normalPaths.begin(), normalPaths.end());
			for (auto &&path : normalPaths) {
				Texture tex = {0, "material.normal", path};
				processedMesh.AddTexture(tex);
			}

			meshes.push_back(processedMesh);
		}

		std::unordered_map<std::string, Image> images;

		// Convert the filename Set into a Vector for passing to loadImageTextures
		std::vector<std::string> imageFilenameVec;
		imageFilenameVec.reserve(imageFilenameSet.size());

		for (auto &&filename : imageFilenameSet)
			imageFilenameVec.push_back(filename);

		images = loadImageTextures(directory, imageFilenameVec);

		// ApplyTexture (which creates actual OpenGL textures) has to be called in the main thread
		for (auto &mesh : meshes) {
			for (Texture &texture : mesh.GetTextures()) {
				if (images.find(texture.path) != images.end()) {
					ApplyTexture(texture, images.at(texture.path));
				}
			}
		}

		for (auto &&image : images) {
			stbi_image_free(image.second.data);
		}

		for (unsigned int i = 0; i < node->mNumChildren; i++) {
			std::vector<Mesh> returnedMeshes = processNode(node->mChildren[i], scene, directory);
			meshes.insert(meshes.end(), returnedMeshes.begin(), returnedMeshes.end());
		}

		return meshes;
	}
	Mesh FileIO::processMesh(aiMesh *mesh, const aiScene *scene, std::string directory)
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<Texture> textures;

		// walk through each of the mesh's vertices
		for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
			Vertex vertex;
			glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that
							  // doesn't directly convert to glm's vec3 class so we transfer the data to this
							  // placeholder glm::vec3 first.
			// positions
			vector.x		= mesh->mVertices[i].x;
			vector.y		= mesh->mVertices[i].y;
			vector.z		= mesh->mVertices[i].z;
			vertex.Position = vector;

			// normals
			if (mesh->HasNormals()) {
				vector.x	  = mesh->mNormals[i].x;
				vector.y	  = mesh->mNormals[i].y;
				vector.z	  = mesh->mNormals[i].z;
				vertex.Normal = vector;
			}

			if(mesh->mColors[0])
			{
				glm::vec3 colour =glm::vec3(0,0,0);
				colour.x = mesh->mColors[0][i].r;
				colour.y = mesh->mColors[0][i].g;
				colour.z = mesh->mColors[0][i].b;
				vertex.Color = colour;
			}
			else
			{
				vertex.Color = glm::vec3(0.0f, 0.0f, 0.0f);
			}
			// texture coordinates
			if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
			{
				glm::vec2 vec;

				vec.x			 = mesh->mTextureCoords[0][i].x;
				vec.y			 = mesh->mTextureCoords[0][i].y;
				vertex.TexCoords = vec;
				// tangent
				vector.x	   = mesh->mTangents[i].x;
				vector.y	   = mesh->mTangents[i].y;
				vector.z	   = mesh->mTangents[i].z;
				vertex.Tangent = vector;
				// bitangent
				vector.x		 = mesh->mBitangents[i].x;
				vector.y		 = mesh->mBitangents[i].y;
				vector.z		 = mesh->mBitangents[i].z;
				vertex.Bitangent = vector;
			} else{
				vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			}

			vertices.push_back(vertex);
		}
		// now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the
		// corresponding vertex indices.
		for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
			aiFace face = mesh->mFaces[i];
			// retrieve all indices of the face and store them in the indices vector
			for (unsigned int j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}
		return Mesh(vertices, indices, textures);
	}

	std::vector<std::string> FileIO::getTexturePaths(aiMaterial *mat, aiTextureType type)
	{
		std::vector<std::string> result;
		result.reserve(mat->GetTextureCount(type));

		for (unsigned int i = 0; i < mat->GetTextureCount(type); i++) {
			aiString aiPath;
			mat->GetTexture(type, i, &aiPath);
			std::string path = aiPath.C_Str();
			std::replace(path.begin(), path.end(), '\\', '/');
			result.push_back(path);
		}
		return result;
	}

	std::optional<FileIO::Image> FileIO::loadImageTexture(std::string directory, std::string filename)
	{
		std::optional<FileIO::Image> result = {};
		int w, h, comp;
		std::string filepath =
			(std::filesystem::path(filename).is_absolute()) ? filename : directory + '/' + filename;
		unsigned char *data = stbi_load(filepath.c_str(), &w, &h, &comp, 0);

		if (data) {
			FileIO::Image image;
			image.data		   = data;
			image.height	   = h;
			image.width		   = w;
			image.nrComponents = comp;
			result			   = image;
		} else {
			spdlog::error("Could not load {0}",filename);
		}

		return result;
	}

	std::unordered_map<std::string, FileIO::Image> FileIO::loadImageTextures(std::string directory,
																			 std::vector<std::string> filenames)
	{
		std::unordered_map<std::string, Image> result;
		std::mutex imagesMutex;
		std::atomic<int> currentPathIndex(0);
		std::vector<std::thread> workers;

		size_t parallelThreadCount = std::min<size_t>(std::thread::hardware_concurrency(), filenames.size());

		for (size_t ii = 0; ii < parallelThreadCount; ++ii) {
			workers.push_back(std::thread([&]() {
				int index;
				while ((index = currentPathIndex.fetch_add(1)) < filenames.size()) {
					std::optional<FileIO::Image> image = loadImageTexture(directory, filenames.at(index));
					if (image.has_value()) {
						std::lock_guard<std::mutex> lock(imagesMutex);
						result.insert({filenames.at(index), image.value()});
					}
				}
			}));
		}

		for (auto &&thread : workers) {
			thread.join();
		}

		return result;
	}

#pragma endregion ObjectLoading

#pragma region TextureLoading

	void FileIO::ApplyTexture(Texture &texture, FileIO::Image imageFile)
	{
		glGenTextures(1, &texture.id);

		if (imageFile.data) {
			GLenum format;

			switch (imageFile.nrComponents) {
				case 1:
					format = GL_RED;
					break;
				case 3:
					format = GL_RGB;
					break;
				case 4:
					format = GL_RGBA;
					break;
				default:
					format = GL_RGBA;
					break;
			}

			glBindTexture(GL_TEXTURE_2D, texture.id);
			glTexImage2D(GL_TEXTURE_2D, 0, format, imageFile.width, imageFile.height, 0, format,
						 GL_UNSIGNED_BYTE, imageFile.data);
			glGenerateMipmap(GL_TEXTURE_2D);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		} else {
			spdlog::error("Failed to apply texture: {0}",texture.path );
		}
	}

	unsigned int FileIO::LoadCubeMapTextures(std::vector<std::string> paths)
	{
		if (paths.size() > 6 || paths.size() < 6)
			return -1;
		
		std::vector<std::string> names;
		for(auto fullPath: paths)
		{
			names.push_back(std::filesystem::path(fullPath).filename().string());
		}

		std::string directory = std::filesystem::path(paths.at(0)).parent_path().string();
		auto images			  = loadImageTextures(directory, names);

		unsigned int textureID;
		glGenTextures(1, &textureID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
		int counter = 0;
		for (auto path : names) {
			if (images[path].data) {


				GLenum format;

				switch (images[path].nrComponents) {
					case 1:
						format = GL_RED;
						break;
					case 3:
						format = GL_RGB;
						break;
					case 4:
						format = GL_RGBA;
						break;
					default:
						format = GL_RGBA;
						break;
				}

				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + counter, 0, format, images[path].width,
							 images[path].height, 0, format, GL_UNSIGNED_BYTE, images[path].data);
				counter++;
			} else {
				return -1;
			}
		}

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		return textureID;
	}

#pragma endregion TextureLoading
} // namespace RenderEngine
