#include "Engine.hpp"
#include <Math/Transformations.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Base/Logic.hpp>
#include <memory>
#include <spdlog/spdlog.h>

// Singeltons
RenderEngine::Engine *RenderEngine::Engine::instance = 0;
namespace RenderEngine
{

	Engine::Engine() {}

	Engine *Engine::Init(Logic *newlogic, int width, int height, const char *name)
	{
		logic		   = newlogic;
		Window *window = Window::GetInstance();
		window->Init(width, height, name);
		camera =  Camera();
		Renderer::GetInstance()->Init();
		m_Light = new Light(4);
		postProcessingPresets = PostProcessingPresets();

		auto shader					= ShaderProgram("Resources/Shaders/light.vs", "Resources/Shaders/light.fs");
		ShaderHandler shaderHandler = ShaderHandler("BaseShader", shader, true);

		AddShaderHandler(shaderHandler);
		instanceShader = ShaderProgram("Resources/Shaders/instance_shader.vs", "Resources/Shaders/instance_shader.fs");


		glGenTextures(1, &renderTarget.id);
		glBindTexture(GL_TEXTURE_2D, renderTarget.id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		logic->Init();

		return this;
	}

	void Engine::ResetEngine()
	{
		Renderer::GetInstance()->EnableSkyBox(false);
		
		delete (m_Light);
		delete (logic);
	}
	std::shared_ptr<ShaderHandler>  Engine::AddShaderHandler(ShaderHandler shaderHandler)
	{

		std::shared_ptr<ShaderHandler> shader = std::make_shared<ShaderHandler>(shaderHandler);
		if (GetShaderHandlerByName(shaderHandler.GetName()) == nullptr) {
			this->renderObjects[shader];
			return shader;
		}
		return nullptr;
	}
	std::shared_ptr<ShaderHandler> Engine::GetShaderHandlerByName(std::string name)
	{
		for (auto x : this->renderObjects) {
			if (x.first->GetName() == name) {
				return x.first;
			}
		}
		return nullptr;
	}

	bool Engine::ShouldClose() { return glfwWindowShouldClose(Window::GetInstance()->GetWindow()); }

	std::shared_ptr<RenderObject> Engine::GetObjectByName(std::string name)
	{
		for (auto &mapPart : renderObjects) {
			for (auto it = mapPart.second.begin(); it != mapPart.second.end(); it++) {
				if ((*it)->GetName() == name)
					return *it;
			}
		}
		return std::shared_ptr<RenderObject>(nullptr);
	}

	void Engine::RenderToTexture(Camera* cam, Texture* target, glm::vec2 textureSize){
				Renderer::GetInstance()->Render(renderObjects, &instanceObjects, m_Light, &instanceShader,target, cam, postProcessingPresets, textureSize);
				glBindTexture(GL_TEXTURE_2D, target->id);
				glGenerateMipmap(GL_TEXTURE_2D);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	void Engine::Render()
	{
		Renderer::GetInstance()->Render(renderObjects, &instanceObjects, m_Light, &instanceShader,&renderTarget, &camera,postProcessingPresets);
		Renderer::GetInstance()->RenderToScreen(renderTarget, postProcessingPresets);
		if (logic != nullptr) {
			Renderer::GetInstance()->RenderImGUI(logic);
		}
		glfwSwapBuffers(Window::GetInstance()->GetWindow());
		glfwPollEvents();
	}

	Camera *Engine::GetCamera() { return &camera; }
	void Engine::SetCamera(Camera camera) { this->camera = camera; }

	void Engine::SetLight(Light *light) { m_Light = light; }

	void Engine::Update()
	{
		float time	   = glfwGetTime() - lastRenderTime;
		lastRenderTime = glfwGetTime();
		if (logic != nullptr) {

			logic->Update(time);
		}

		for(auto x : particles)
		{
			x->Update(time);
		}

		//	fpsCounter->Update();
		if (limitFrame) {
			Sync();
		}
	}

	bool Engine::AddObject(std::shared_ptr<RenderObject>object)
	{
		if (object->GetName() == "" || GetObjectByName(object->GetName()) == nullptr) {
			renderObjects[object->GetShaderHandler()].push_back(object);
			return true;
		}

		return false;
	}

	bool Engine::RemoveObject(std::shared_ptr<RenderObject>object)
	{
		
		renderObjects[object->GetShaderHandler()].erase(
			std::remove(renderObjects[object->GetShaderHandler()].begin(),
						renderObjects[object->GetShaderHandler()].end(), object),
			renderObjects[object->GetShaderHandler()].end());
			
		return true;
	}

	std::vector<std::shared_ptr<RenderObject>> Engine::GetAllObjects()
	{

		std::vector<std::shared_ptr<RenderObject>> obj;
		for (auto part : renderObjects) {
			obj.insert(obj.end(), part.second.begin(), part.second.end());
		}
		return obj;
	}

	Engine *Engine::GetInstance()
	{
		if (!instance) {
			spdlog::debug("New Engine created");
			instance = new Engine();
		}
		return instance;
	}
	void Engine::SetNewLogic(Logic *logic) {
		ResetEngine();
		this->logic = logic;
	}
	Engine::~Engine() { ResetEngine(); }

	void Engine::LimitFrameRate(bool sync, int FPSTime)
	{
		this->limitFrame = sync;
		this->targetFPS	 = FPSTime;
	}
	Light *Engine::GetLight() { return m_Light; }
	void Engine::Sync()
	{

		if (lastTime != -1) {
			double targetTime = 1.0f / targetFPS;
			double spendTime  = 0;
			lastTime		  = glfwGetTime();
			while (targetTime > spendTime) {
				frameCounter++;
				double currentTime = glfwGetTime();
				spendTime += currentTime - lastTime;
				timeCounter += currentTime - lastTime;

				if (timeCounter >= 1) {
					frameCounter = 0;
					timeCounter	 = 0;
				}
				lastTime = currentTime;
			}

		} else {
			lastTime = glfwGetTime();
		}
	}
	void Engine::AddInstancedObject(std::shared_ptr<InstanceObject>object) { instanceObjects.push_back(object); }

}
