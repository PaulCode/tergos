set (_tmp_headers

    Graphics/Object/ParticleObjects/ParticleObject.hpp
    Graphics/Object/ParticleObjects/ParticleGravityAffected.hpp
    Graphics/Object/ParticleObjects/ExplodingParticle.cpp

    Base/Camera.hpp
    Math/Transformations.hpp
    Base/Logic.hpp
    Base/Renderer.hpp
    Base/Window.hpp
    Graphics/Light/Light.hpp
    Graphics/Object/Mesh.hpp
    Graphics/Object/RenderObject.hpp
    Graphics/Object/InstanceObject.hpp
    Graphics/ShaderProgram/ShaderProgram.hpp
    Graphics/ShaderProgram/ShaderHandler.hpp
    Graphics/PPP.hpp
    Graphics/Object/SkyBox.hpp
    Utils/Image/stb_image.hpp
    Utils/TimeKeeper.hpp
    Utils/FileIO.hpp
    Utils/Input.hpp
    Engine.hpp
    Utils/MeshGenerator.hpp


    Graphics/imgui/imconfig.h
    Graphics/imgui/imgui.h
    Graphics/imgui/imgui_impl_glfw.h
    Graphics/imgui/imgui_impl_opengl3.h
    Graphics/imgui/imgui_internal.h
    Graphics/imgui/imstb_rectpack.h
    Graphics/imgui/imstb_textedit.h
    Graphics/imgui/imstb_truetype.h


    )

set (_tmp_sources
    Graphics/Object/ParticleObjects/ParticleObject.cpp
    Graphics/Object/ParticleObjects/ParticleGravityAffected.cpp
    Graphics/Object/ParticleObjects/ExplodingParticle.cpp


    Base/Camera.cpp
    Math/Transformations.cpp
    Base/Renderer.cpp
    Base/Window.cpp
    Graphics/Light/Light.cpp
    Graphics/Object/Mesh.cpp
    Graphics/Object/RenderObject.cpp
    Graphics/Object/InstanceObject.cpp
    Graphics/ShaderProgram/ShaderProgram.cpp
    Graphics/PPP.cpp
    Graphics/Object/SkyBox.cpp
    Utils/FileIO.cpp
    Utils/Input.cpp
    Engine.cpp
    Utils/MeshGenerator.cpp


    Graphics/imgui/imgui.cpp
    Graphics/imgui/imgui_demo.cpp
    Graphics/imgui/imgui_draw.cpp
    Graphics/imgui/imgui_tables.cpp
    Graphics/imgui/imgui_widgets.cpp
    Graphics/imgui/imgui_impl_glfw.cpp
    Graphics/imgui/imgui_impl_opengl3.cpp

    )

get_filename_component(CURRENT_PARENT_DIR ${CMAKE_CURRENT_SOURCE_DIR} PATH)
if(ENGINE_LINK_RESOURCES)
    # Directory that Engine resources can be found.
    set(ENGINE_RESOURCES_DIR "${CURRENT_PARENT_DIR}/Resources")
endif()
if(ENGINE_INSTALL_RESOURCES)
    # Install resources for end-user usage because many source files use these
    install(DIRECTORY "${CURRENT_PARENT_DIR}/Resources"
            # Example: this will install the Resources dir to /usr/share/Engine/Resources on Linux
            DESTINATION "${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}"
            )
    
    file(COPY "${CURRENT_PARENT_DIR}/Resources"
        DESTINATION "${CMAKE_BINARY_DIR}/bin"
        )
endif()

# Sets all headers as PUBLIC sources for Engine
# The BUILD/INSTALL interface generator expressions are for the EXPORT command
# Otherwise it wouldn't know where to look for them
foreach(_engine_header IN LISTS _tmp_headers)
    target_sources(Engine PRIVATE
            $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${_engine_header}>
            $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${_engine_header}>
            )
endforeach()

# Sets all sources (cpp) as PRIVATE sources for Engine
# An INSTALL_INTERFACE isn't needed, as cpp files aren't installed
foreach(_engine_source IN LISTS _tmp_sources)
    target_sources(Engine PRIVATE
            $<BUILD_INTERFACE:${_engine_source}>
            )
endforeach()

# Include this file in our project view.
target_sources(Engine PRIVATE
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/CMakeSources.cmake>
        )
