#include "Light.hpp"

namespace RenderEngine
{
	Light::Light(DirLight &dirLight, PointLight &pointLight, SpotLight &spotLight)
	{
		m_DirLights	  = {dirLight};
		m_PointLights = {pointLight};
		m_SpotLights  = {spotLight};
	}

	Light::Light(std::vector<DirLight> &dirLights, std::vector<PointLight> &pointLights,
				 std::vector<SpotLight> &spotLights)
		: m_DirLights(dirLights)
		, m_PointLights(pointLights)
		, m_SpotLights(spotLights)
	{
	}

	Light::Light(int count)
		: lightCount(count)
	{
		for (int i = 0; i < count; i++) {
			m_DirLights.push_back(
				DirLight {glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), 1});

			m_PointLights.push_back(PointLight {
				glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), 0, 0, 0, 1
			});

			SpotLight *spotLight = new SpotLight(
				glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), 0, 0, 0, 0, 0, 1
			);
			m_SpotLights.push_back(*spotLight);
		}
	}

	Light::Light() {}

	Light::~Light() {}

	void Light::AddDirLight(DirLight &dirLight) {
		for (int i = 0; i < m_DirLights.size(); i++) {
			if (m_DirLights[i].filler) {
				m_DirLights[i] = dirLight;
			}
		}
	}

	void Light::AddPointLight(PointLight &dirLight) {
		for (int i = 0; i < m_PointLights.size(); i++) {
			if (m_PointLights[i].filler) {
				m_PointLights[i] = dirLight;
			}
		}
	}
	
	void Light::AddSpotLight(SpotLight &dirLight) {
		for (int i = 0; i < m_SpotLights.size(); i++) {
			if (m_SpotLights[i].filler) {
				m_SpotLights[i] = dirLight;
			}
		}
	}

	void Light::Update(ShaderProgram &shader, Camera &camera)
	{

		shader.setVec3("viewPos", camera.GetPosition());
		shader.setVec3("defaultAmbient", glm::vec3(0.25f, 0.25f, 0.25f));

		// directional light
		for (int i = 0; i < m_DirLights.size(); ++i) {
			auto dirLight = m_DirLights.at(i);
			std::ostringstream sstream;
			sstream << "dirLights[" << i << "].";
			std::string query = sstream.str();

			shader.setVec3(query + "direction", dirLight.direction);
			shader.setVec3(query + "ambient", dirLight.ambient);
			shader.setVec3(query + "diffuse", dirLight.diffuse);
			shader.setVec3(query + "specular", dirLight.specular);
			shader.setInt(query + "filler", dirLight.filler);
		}

		// point light
		for (int i = 0; i < m_PointLights.size(); ++i) {
			auto pointLight = m_PointLights.at(i);
			std::ostringstream sstream;
			sstream << "pointLights[" << i << "].";
			std::string query = sstream.str();

			shader.setVec3(query + "position", pointLight.position);
			shader.setVec3(query + "ambient", pointLight.ambient);
			shader.setVec3(query + "diffuse", pointLight.diffuse);
			shader.setVec3(query + "specular", pointLight.specular);
			shader.setFloat(query + "constant", pointLight.constant);
			shader.setFloat(query + "linear", pointLight.linear);
			shader.setFloat(query + "quadratic", pointLight.quadratic);
			shader.setInt(query + "filler", pointLight.filler);
		}

		// spotLight
		for (int i = 0; i < m_SpotLights.size(); ++i) {
			auto spotLight = m_SpotLights.at(i);
			std::ostringstream sstream;
			sstream << "spotLights[" << i << "].";
			std::string query = sstream.str();

			shader.setVec3(query + "position", camera.GetPosition());
			shader.setVec3(query + "direction", camera.GetDirection());
			shader.setVec3(query + "ambient", spotLight.ambient);
			shader.setVec3(query + "diffuse", spotLight.diffuse);
			shader.setVec3(query + "specular", spotLight.specular);
			shader.setFloat(query + "constant", spotLight.constant);
			shader.setFloat(query + "linear", spotLight.linear);
			shader.setFloat(query + "quadratic", spotLight.quadratic);
			shader.setFloat(query + "cutOff", spotLight.cutOff);
			shader.setFloat(query + "outerCutOff", spotLight.outerCutOff);
			shader.setInt(query + "filler", spotLight.filler);
		}
	}
}