#pragma once

#include "Graphics/ShaderProgram/ShaderProgram.hpp"
#include "Base/Camera.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

namespace RenderEngine
{

	/**
	 * @brief Directional Light
	 * 
	 */
	struct DirLight {
		glm::vec3 direction;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
        int filler = 0; // ignore
	};
	

	/**
	 * @brief Point Light
	 * 
	 */
	struct PointLight {
		glm::vec3 position;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
		float constant;
		float linear;
		float quadratic;
        int filler = 0; // ignore
	};
	/**
	 * @brief Spot Ligt
	 * 
	 */
	struct SpotLight {
		glm::vec3 position;
		glm::vec3 direction;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
		float constant;
		float linear;
		float quadratic;
		float cutOff;
		float outerCutOff;
        int filler; // ignore

		SpotLight(glm::vec3 position, glm::vec3 direction, glm::vec3 ambient, glm::vec3 diffuse,
				  glm::vec3 specular, float constant, float linear, float quadratic, float cutOff,
				  float outerCutOff, int filler = 0)
			: position(position)
			, direction(direction)
			, ambient(ambient)
			, diffuse(diffuse)
			, specular(specular)
			, constant(constant)
			, linear(linear)
			, quadratic(quadratic)
			, cutOff(cutOff)
			, outerCutOff(outerCutOff)
            , filler(filler)
		{
		}
	};



	/**
	 * @brief used to handle lights, part of Engine
	 * 
	 */
	class Light
	{
	public:
		Light(DirLight &dirLight, PointLight &pointLight, SpotLight &spotLight);
		Light();
        Light(int count);
		Light(std::vector<DirLight> &dirLight, std::vector<PointLight> &pointLight,
			  std::vector<SpotLight> &spotLight);
		~Light();

		void Update(ShaderProgram &shader, Camera &camera);

		void AddDirLight(DirLight &dirLight);
		void AddPointLight(PointLight &dirLight);
		void AddSpotLight(SpotLight &dirLight);


		std::vector<PointLight>& GetPointLights(){
			return m_PointLights;
		}

	private:
        int lightCount = 4;
		std::vector<DirLight> m_DirLights;
		std::vector<PointLight> m_PointLights;
		std::vector<SpotLight> m_SpotLights;

		friend class Renderer;
	};
}