#pragma once

#include <glm/glm.hpp>
#include <Graphics/ShaderProgram/ShaderProgram.hpp>
namespace RenderEngine
{
	

	struct Bloom{
		bool useBloom = false;
		float directions = 16.0; // BLUR DIRECTIONS (Default 16.0 - More is better but slower)
		float quality = 3.0; // BLUR QUALITY (Default 4.0 - More is better but slower)
		float size = 16.0;    // BLUR SIZE (Radius)
	};
	struct Vignette{
		bool isUsed = false;
		float fallOff = 0.5;
		float amount = 0.5;
		float offset1 = 0.5;
    	float offset2 = 0.5;

	};

	struct Blur {
		bool isUsed = false;
		float radius = 0.1f;
	};

	struct ColorCorrection {
		bool isUsed = false;
		glm::vec3 colorCorr = {0.0,0.0,0.0};
		float exposure;
		float contrast;
		float saturation;
	};

	class PostProcessingPresets
	{
	public:
		// Gets
		const Bloom& GetBloom() const;
		Blur* GetBlur();
		bool GetColorCorrection();
		ColorCorrection* GetColorCorr();
		bool GetToneMapping();
		int GetIntensity();
		float GetGammaCorrection();
		bool GetHasGammaCorrection();
		Vignette* GetVignette();
		// Sets
		void SetBloom(bool bloom);
		void SetHasGammaCorrection(bool hasGamma);
		void SetShaderVariables(ShaderProgram* ShaderProgram);
		void SetBloom(Bloom bloom);
		void SetVignette(Vignette vign);
		void SetBlur(Blur blur);
		void SetColorCorr(ColorCorrection ColorCorrection);
		void SetToneMapping(bool hasToneMapping, int intensity);
		void SetGammaCorrection(float gamma);
	private:
		Bloom bloom;
		Vignette vignet;
		Blur blur;
		bool hasToneMapping =false;
		bool hasGammaCorrection = false;
		float gammaCorrection = 2.2f;
		float VignetteIntensity = 0;
		ColorCorrection colorCorr;
		
	};
}