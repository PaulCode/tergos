#include "PPP.hpp"
#include "Graphics/ShaderProgram/ShaderProgram.hpp"
#include <Base/Window.hpp>

namespace RenderEngine
{

	void PostProcessingPresets::SetHasGammaCorrection(bool hasGamma){
		this->hasGammaCorrection = hasGamma;
	}

	bool PostProcessingPresets::GetHasGammaCorrection(){
		return hasGammaCorrection;
	}

	void PostProcessingPresets::SetBloom(Bloom bloom){
		this->bloom = bloom;
	}

	float PostProcessingPresets::GetGammaCorrection(){
		return gammaCorrection;
	}
	void PostProcessingPresets::SetGammaCorrection(float val)
	{
		this->gammaCorrection = val;
	}
	const Bloom& PostProcessingPresets::GetBloom() const { return bloom; }

	void PostProcessingPresets::SetBlur(Blur blur)
	{
		this->blur = blur;
	}
	Blur* PostProcessingPresets::GetBlur() { return &blur; }


	void PostProcessingPresets::SetColorCorr(ColorCorrection c)
	{
		this->colorCorr			 = colorCorr;
	}

	void PostProcessingPresets::SetToneMapping(bool toneMappinprig, int intensity)
	{
		this->hasToneMapping = toneMappinprig;
	}
	bool PostProcessingPresets::GetToneMapping() { return hasToneMapping; }

	ColorCorrection* PostProcessingPresets::GetColorCorr() { return &colorCorr; }


	Vignette* PostProcessingPresets::GetVignette(){
		return &vignet;
	}

	void PostProcessingPresets::SetVignette(Vignette vign)
	{
		this->vignet = vign;
	}

	void PostProcessingPresets::SetShaderVariables(ShaderProgram* shaderProgram){
		shaderProgram->setBool("vignette.isUsed", vignet.isUsed);
		shaderProgram->setFloat("vignette.fallOff", vignet.fallOff);
		shaderProgram->setFloat("vignette.amount", vignet.amount);
		shaderProgram->setFloat("vignette.offset1", vignet.offset1);
		shaderProgram->setFloat("vignette.offset2", vignet.offset2);

		shaderProgram->setBool("blur.isUsed", blur.isUsed);
		shaderProgram->setFloat("blur.radius", blur.radius);

		shaderProgram->setBool("hasBloom", bloom.useBloom);

		shaderProgram->setBool("hasToneMapping", hasToneMapping);
		shaderProgram->setBool("colorCorr.isUsed", colorCorr.isUsed);
		shaderProgram->setVec3("colorCorr.colorCorr", colorCorr.colorCorr);
		shaderProgram->setFloat("colorCorr.exposure", colorCorr.exposure);
		shaderProgram->setFloat("colorCorr.contrast", colorCorr.contrast);
		shaderProgram->setFloat("colorCorr.saturation",colorCorr.saturation);
		shaderProgram->setBool("hasGammaCorrection", hasGammaCorrection);
		shaderProgram->setFloat("gammaCorrection", gammaCorrection);

		auto window = Window::GetInstance();
		shaderProgram->setVec2("screenSize", {window->GetWidth(), window->GetHeigth()});
	}

	/*
		bool hasBloom = false;
		bool hasBlur = false;
		bool hasColorCorrection = false;
		bool hasVignette = false;
		bool hasToneMapping =false;
		int intensity;
		glm::vec3 colorCorr = {0,0,0};
	*/


}

