#include "ShaderProgram.hpp"
#include <stdio.h>
#include <spdlog/spdlog.h>

namespace RenderEngine
{
	ShaderProgram::~ShaderProgram() {}

	int ShaderProgram::getShaderProgram() { return shaderProgram; }

	ShaderProgram::ShaderProgram(std::string vs, std::string fs)
	{
		this->shaderProgram = CompileShader(vs.c_str(), fs.c_str());
	}
	ShaderProgram::ShaderProgram(const char *vertexPath, const char *fragmentPath)
	{
		// 1. retrieve the vertex/fragment source code from filePath
		std::string vertexCode;
		std::string fragmentCode;
		std::string geometryCode;
		std::ifstream vShaderFile;
		std::ifstream fShaderFile;
		std::ifstream gShaderFile;
		// ensure ifstream objects can throw exceptions:
		vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		gShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try {
			// open files
			vShaderFile.open(vertexPath);
			fShaderFile.open(fragmentPath);
			std::stringstream vShaderStream, fShaderStream;
			// read file's buffer contents into streams
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			// close file handlers
			vShaderFile.close();
			fShaderFile.close();
			// convert stream into string
			vertexCode	 = vShaderStream.str();
			fragmentCode = fShaderStream.str();
			// if geometry shader path is present, also load a geometry shader
		} catch (std::ifstream::failure _) {
			spdlog::error("Shader not found at location: {0}", vertexPath);
		}
		const char *vShaderCode = vertexCode.c_str();
		const char *fShaderCode = fragmentCode.c_str();

		// vertex shader
		this->shaderProgram = CompileShader(vShaderCode, fShaderCode);
	}

	unsigned int ShaderProgram::CompileShader(const char *vShaderCode, const char *fShaderCode)
	{
		unsigned int vertex, fragment;
		// vertex shader
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		checkCompileErrors(vertex, "VERTEX");
		// fragment Shader
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		checkCompileErrors(fragment, "FRAGMENT");

		unsigned int shaderCode = glCreateProgram();
		glAttachShader(shaderCode, vertex);
		glAttachShader(shaderCode, fragment);
		glLinkProgram(shaderCode);
		int error = checkCompileErrors(shaderCode, "PROGRAM");
		glDeleteShader(vertex);
		glDeleteShader(fragment);
		if (error) {
			std::string vertex	 = " #version 330 core \n  void main(){}";
			std::string fragment = "#version 330 core \n  void main(){}";

			shaderCode = CompileShader(vertex.c_str(), fragment.c_str());
		}
		return shaderCode;
	}
	int ShaderProgram::onError() { return 0; }
	int ShaderProgram::createShader(const char *shaderCode, int shaderType)
	{
		int success;
		char infoLog[512];
		int shader = glCreateShader(shaderType);
		glShaderSource(shader, 1, &shaderCode, NULL);
		glCompileShader(shader);
		// check for shader compile errors
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(shader, 512, NULL, infoLog);
			spdlog::error("Fragement Shader Compilation Error \n {0} \n", infoLog);
		}

		return shader;
	}

	int ShaderProgram::checkCompileErrors(GLuint shader, std::string type)
	{
		GLint success;
		GLchar infoLog[1024];
		if (type != "PROGRAM") {
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(shader, 1024, NULL, infoLog);
				spdlog::error("Shader Compilation of type: {0} \n {1} \n  -- "
							  "--------------------------------------------------- -- ",
							  type, infoLog);
				return -1;
			}
		} else {
			glGetProgramiv(shader, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(shader, 1024, NULL, infoLog);
				spdlog::error("Shader Linking error of type: {0} \n {1} \n  -- "
							  "--------------------------------------------------- -- ",
							  type, infoLog);

				return -1;
			}
		}
		return 0;
	}
	void ShaderProgram::useShader() { glUseProgram(shaderProgram); }

} // namespace RenderEngine