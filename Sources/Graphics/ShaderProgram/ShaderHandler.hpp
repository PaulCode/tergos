#pragma once
#include "ShaderProgram.hpp"
#include <Graphics/Light/Light.hpp>
#include <Base/Camera.hpp>
#include <Math/Transformations.hpp>
#include "glm/gtx/string_cast.hpp"

#include <string>
#include <stdio.h>
namespace RenderEngine
{
	/**
	 * @brief The ShaderHandler is used to manage shaders, to add a new Shader create a shaderHandler and pass a shaderProgram to it. then add it Engine using AddShaderHandler; A simple Shader can be seen in Resources/Shaders/vs_shader.vs and fr_shader.fs
	 * 
	 */
	class ShaderHandler
	{
	public:
		ShaderHandler(){}
		ShaderHandler(std::string name, ShaderProgram shaderProgram, bool useLight)
		{
			this->Name		= name;
			this->shaderPr	= shaderProgram;
			this->useLights = useLight;
		}
		~ShaderHandler(){

        }

		/**
		 * @brief used by the renderer 
		 * 
		 * @param light 
		 * @param camera 
		 */
		void BindShader(Light *light, Camera *camera)
		{
			GetShaderProgram()->useShader();
			this->SetGlobalVariables(light, camera);
		}
		std::string GetName(){
        return Name;
    }
	ShaderProgram *GetShaderProgram(){
        return &shaderPr;
    }

	private:
		/**
		 * @brief Can be overriden, used to set global variables inside the shader like the light or the projection and view matrix.
		 * 
		 * @param light 
		 * @param camera 
		 */
		virtual void SetGlobalVariables(Light *light, Camera *camera)
		{
			if(light != nullptr && useLights)
			{
				ShaderProgram* shader = &shaderPr;
				light->Update(*shader, *camera);
			}
			shaderPr.setMat4("projection", camera->getProjectionMatrix());
			shaderPr.setMat4("view", camera->GetViewMatrix());
		}
		std::string Name;
		ShaderProgram shaderPr;
		bool useLights;
	};
}
