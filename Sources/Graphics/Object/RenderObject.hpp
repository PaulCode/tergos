#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include <memory>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Math/Transformations.hpp>

#include "Mesh.hpp"
#include <Graphics/ShaderProgram/ShaderProgram.hpp>
#include <Utils/FileIO.hpp>
#include <Graphics/ShaderProgram/ShaderHandler.hpp>

namespace RenderEngine
{
	class Renderer;

	/**
	 * @brief A renderObjects stores one or more meshes, and the position,scale and rotation of these
	 *
	 * \code{.cpp}
	 * // load a object into a renderObject
	 * 		RenderObject *object = new RenderObject("object");
	 * 		object->SetName("firstObject");
	 * 		Engine::GetInstance()->AddObject(object);
	 * \endcode
	 *
	 */
	class RenderObject
	{
	public:
		std::vector<Mesh> meshes;
		bool gammaCorrection;

		RenderObject(std::string const &path);
		RenderObject(Mesh mesh);
		RenderObject();
		~RenderObject();
		
		RenderObject(std::string const &path, std::shared_ptr<ShaderHandler> shaderHandler);
		RenderObject(Mesh mesh, std::shared_ptr<ShaderHandler> shaderHandler);
		RenderObject(std::shared_ptr<ShaderHandler> shaderHandler);

		void AddMesh(Mesh mesh);
		void SetName(std::string name);
		std::string GetName();

		glm::vec3 GetPosition();
		void SetPosition(glm::vec3 newPos);
		glm::vec3 GetRotation();
		void SetRotation(glm::vec3 newRot);
		void SetScale(glm::vec3 newScale);
		glm::vec3 *GetScale();

		std::shared_ptr<ShaderHandler> GetShaderHandler();

		void SetVisibility(bool visible);
		bool GetVisibility();


		void EnableLight(bool light);
		void EnableVertexColors(bool light);

		float GetBoundingSphereRadius();

	protected:
		std::shared_ptr<ShaderHandler> shaderHandler;
		std::string name;
		bool isVisible = true;
		glm::vec3 position = glm::vec3(0, 0, 0), rotation = glm::vec3(0, 0, 0), scale = glm::vec3(1, 1, 1);


		/**
		 * @brief Can be override, sets Local Shader Variables.
		 * 
		 */
		virtual void SetLocalShaderVariables();
		void LoadFromFile(std::string const &path);


		void Render(ShaderProgram *shaderProgram, unsigned int cubeMapTextureID);

		friend Renderer;
	};
} // namespace RenderEngine
