#include "ParticleGravityAffected.hpp"


namespace RenderEngine{
    void ParticleGravityAffected::Update(float deltaTime){
        double timeDiff = glfwGetTime() - lastTime;
        lastTime = glfwGetTime();

        std::default_random_engine generator;

        std::uniform_real_distribution<double> distribution2(0,lifeTime);

        for(int i = 0; i < this->size; i++)
        {

                speed[i] -=  2.31 * deltaTime;
                float updateY = speed[i] / 13;

                this->instanceObject->data[i].position = this->instanceObject->data[i].position + glm::vec3( moveDir[i].x, updateY,moveDir[i].z);
                this->instanceObject->data[i].lifeTime = this->instanceObject->data[i].lifeTime -= timeDiff;
                if(this->instanceObject->data[i].lifeTime <= 0)
                {
                    this->instanceObject->data[i].lifeTime = distribution2(generator);
                    this->instanceObject->data[i].position = startPosition;

                    
                    speed[i] = speedV;
                }
        }

        this->instanceObject->SetBuffer();
    }
}