#pragma once
#include "ParticleObject.hpp"
#include "glm/fwd.hpp"


#include <time.h>
#include <iostream>
#include <spdlog/spdlog.h>

#include <random>

namespace RenderEngine {
    class ExplodingParticle : public ParticleObject{
        public:
            ExplodingParticle(int size,std::shared_ptr<RenderObject> object,float spreadValue = 7,float lifeTime = 10, glm::vec3 pos = {0,0,0}) : ParticleObject(size,object,lifeTime,pos){
                this->spread = spreadValue;

                this->countdown = 3;
                this->coolDown = lifeTime;

                moveDir = new glm::vec3[size];

                std::default_random_engine generator;
                std::uniform_real_distribution<double> distribution(-spreadValue,spreadValue);


                for(int i = 0; i < size;i++){
                    float updateX = distribution(generator) / 13; 
                    float updateY = distribution(generator) /13;
                    float updateZ = distribution(generator) / 13; 

                    moveDir[i] = glm::vec3(updateX,updateY,updateZ);
                }
            }

            float GetRadius(){return spread;}
            void SetRadius(float spreadRadius)
            {
                this->spread = spreadRadius;
                std::default_random_engine generator;
                std::uniform_real_distribution<double> distribution(-spread,spread);


                for(int i = 0; i < size;i++){
                    float updateX = distribution(generator) / 13; 

                    float updateZ = distribution(generator) / 13; 

                    moveDir[i] = glm::vec3(updateX,0,updateZ);
                }
            }


        private:   
        void Update(float deltaTime) override;   

        float countdown;


        float coolDown;

        float spread; 
        glm::vec3* moveDir;
    };
}