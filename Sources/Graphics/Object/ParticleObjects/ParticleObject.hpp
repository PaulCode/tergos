#pragma once
#include "Graphics/Object/InstanceObject.hpp"
#include <memory.h>
#include <memory>
#include "Graphics/Object/RenderObject.hpp"
#include "glm/fwd.hpp"
#include <GLFW/glfw3.h>

namespace RenderEngine {
    class ParticleObject{
        public:
            ParticleObject() = default; 
            ParticleObject(int size,std::shared_ptr<RenderObject> object, float lifeTime = 10, glm::vec3 pos = {0,0,0});
            ~ParticleObject();
            virtual void Update(float deltaTime);

            float GetLifeTime(){return lifeTime;}
            void SetLifeTime(float lifeTime){this->lifeTime = lifeTime;}
        glm::vec3 startPosition;

        protected:
        int size;
        float lifeTime;
        std::shared_ptr<InstanceObject> instanceObject;
        double lastTime = -1; 
    };
}