#include "ExplodingParticle.hpp"
#include "spdlog/spdlog.h"


namespace RenderEngine{
    void ExplodingParticle::Update(float deltaTime){
        double timeDiff = glfwGetTime() - lastTime;
        lastTime = glfwGetTime();

        std::default_random_engine generator;

        std::uniform_real_distribution<double> distribution2(0,lifeTime);
        


        countdown -= timeDiff;
        bool reset = false;
        int resetCount = 0;
        if(countdown <= 0)
        {
            countdown -= timeDiff;
            for(int i = 0; i < this->size; i++)
            {
                this->instanceObject->data[i].lifeTime = this->instanceObject->data[i].lifeTime -= timeDiff;
                if(this->instanceObject->data[i].lifeTime >= 0)
                {
                    this->instanceObject->data[i].position = this->instanceObject->data[i].position + glm::vec3( moveDir[i].x, moveDir[i].y,moveDir[i].z);                    
                }
                else{
                    this->instanceObject->data[i].position = startPosition;
                    resetCount++;
                }
            }
        }

        spdlog::info("{0}",resetCount);
        
        if(resetCount == size)
        {
            this->countdown = coolDown;
            for(int i = 0; i < this->size; i++)
            {
                this->instanceObject->data[i].lifeTime = distribution2(generator);
                this->instanceObject->data[i].position = startPosition;
            }
        }

        this->instanceObject->SetBuffer();
    }
}