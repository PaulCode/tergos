#include "ParticleObject.hpp"
#include "GLFW/glfw3.h"
#include "Graphics/Object/InstanceObject.hpp"
#include "glm/detail/qualifier.hpp"
#include "glm/fwd.hpp"
#include <Engine.hpp>
#include <stdlib.h>
#include <algorithm>



#include <time.h>
#include <iostream>
#include <spdlog/spdlog.h>

#include <random>


namespace RenderEngine {
    ParticleObject::ParticleObject(int size,std::shared_ptr<RenderObject> object, float lifeTime, glm::vec3 pos){
        this->size = size;
        this->startPosition = pos;
        this->lifeTime = lifeTime;
        instanceObject = std::make_shared<InstanceObject>(object, size);


        for(int i = 0; i < size; i++)
        {
            instanceObject->data[i].lifeTime = lifeTime;
        }

        Engine::GetInstance()->AddInstancedObject(instanceObject);

        lastTime = glfwGetTime();

        srand( (unsigned)time( NULL ) );
    }


    ParticleObject::~ParticleObject(){
        Engine::GetInstance()->instanceObjects.erase(std::remove(Engine::GetInstance()->instanceObjects.begin(), Engine::GetInstance()->instanceObjects.end(), instanceObject), Engine::GetInstance()->instanceObjects.end());
    }

    void ParticleObject::Update(float deltaTime)
    {
        double timeDiff = glfwGetTime() - lastTime;
        lastTime = glfwGetTime();

        std::default_random_engine generator;
        std::uniform_real_distribution<double> distribution(-0.4,0.4);

        std::uniform_real_distribution<double> distribution2(0,lifeTime);

        for(int i = 0; i < this->size; i++)
        {

                float updateX = distribution(generator) / 13; 
                float updateY = (float)rand()/RAND_MAX / 13; 
                float updateZ = distribution(generator) / 13; 

                this->instanceObject->data[i].position = this->instanceObject->data[i].position + glm::vec3( updateX, updateY,updateZ);
                this->instanceObject->data[i].lifeTime = this->instanceObject->data[i].lifeTime -= timeDiff;
                if(this->instanceObject->data[i].lifeTime <= 0)
                {
                    this->instanceObject->data[i].lifeTime = distribution2(generator);
                    this->instanceObject->data[i].position = startPosition;
                }
        }

        this->instanceObject->SetBuffer();
        
    }
}