#include "RenderObject.hpp"
#include <Engine.hpp>

namespace RenderEngine
{

	RenderObject::~RenderObject()
	{
		for (auto mesh : meshes) {
			mesh.CleanUp();
		}
	}

	RenderObject::RenderObject(std::string const &path, std::shared_ptr<ShaderHandler> shaderHandler)
	{
		this->shaderHandler = shaderHandler;
		LoadFromFile(path);
	}
	RenderObject::RenderObject(Mesh mesh, std::shared_ptr<ShaderHandler> shaderHandler)
	{
		this->shaderHandler = shaderHandler;
		AddMesh(mesh);
	}
	RenderObject::RenderObject(std::shared_ptr<ShaderHandler> shaderHandler) { this->shaderHandler = shaderHandler; }

	RenderObject::RenderObject(Mesh mesh) { AddMesh(mesh); }
	RenderObject::RenderObject(std::string const &path)
	{
		this->shaderHandler = Engine::GetInstance()->GetShaderHandlerByName("BaseShader");
		LoadFromFile(path);
	}

	RenderObject::RenderObject() {}

	void RenderObject::LoadFromFile(std::string const &path)
	{
		auto returnedMeshes = FileIO::loadModel(path);

		for (auto it = returnedMeshes.begin(); it != returnedMeshes.end(); it++) {
			meshes.push_back(*it);
		}
	}

	void RenderObject::Render(ShaderProgram *shaderProgram,unsigned int cubeMapTextureID)
	{

		SetLocalShaderVariables();
		for (unsigned int i = 0; i < meshes.size(); i++)
			meshes[i].Render(shaderProgram,cubeMapTextureID);
	}

	void RenderObject::SetLocalShaderVariables()
	{
		glm::mat4 model = Transformations::getModelMatrix(position, rotation, scale);
		shaderHandler->GetShaderProgram()->setMat4("model", model);
	}

	void RenderObject::EnableLight(bool light)
	{
		for (unsigned int i = 0; i < meshes.size(); i++) {
			meshes[i].EnableLight(light);
		}
	}
	void RenderObject::EnableVertexColors(bool light)
	{
		for (unsigned int i = 0; i < meshes.size(); i++) {
			meshes[i].EnableVertexColour(light);
		}
	}

	void RenderObject::AddMesh(Mesh mesh) { meshes.push_back(mesh); }

	glm::vec3 RenderObject::GetPosition() { return position; }

	void RenderObject::SetPosition(glm::vec3 newPos)
	{
		position.x = newPos.x;
		position.y = newPos.y;
		position.z = newPos.z;
	}

	glm::vec3 RenderObject::GetRotation() { return rotation; }

	void RenderObject::SetRotation(glm::vec3 newRot)
	{
		rotation.x = newRot.x;
		rotation.y = newRot.y;
		rotation.z = newRot.z;
	}

	void RenderObject::SetScale(glm::vec3 newScale)
	{
		this->scale.x = newScale.x;
		this->scale.y = newScale.y;
		this->scale.z = newScale.z;
	}

	void RenderObject::SetVisibility(bool visible) { this->isVisible = visible; }
	bool RenderObject::GetVisibility() { return isVisible; }

	void RenderObject::SetName(std::string name) { this->name = name; }
	std::string RenderObject::GetName() { return this->name; }

	std::shared_ptr<ShaderHandler> RenderObject::GetShaderHandler() { return this->shaderHandler; }
	glm::vec3 *RenderObject::GetScale() { return &scale; }

	float RenderObject::GetBoundingSphereRadius()
	{
		float radius = 0;
		for (auto &&mesh : meshes) {
			radius = std::max(radius, mesh.GetBoundingSphereRadius());
		}
		return radius;
	}
}
