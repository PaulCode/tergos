#include "SkyBox.hpp"
#include <GL/glew.h>
#include <Utils/FileIO.hpp>

namespace RenderEngine
{

	void SkyBox::Setup(std::vector<std::string> faces)
	{
		sh = ShaderProgram("Resources/Shaders/skybox.vs", "Resources/Shaders/skybox.fs");
		textureID = FileIO::LoadCubeMapTextures(faces);

		float skyboxVertices[] = {// positions
								  -1.0f, 1.0f,	-1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f,
								  1.0f,	 -1.0f, -1.0f, 1.0f,  1.0f,	 -1.0f, -1.0f, 1.0f,  -1.0f,

								  -1.0f, -1.0f, 1.0f,  -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,  -1.0f,
								  -1.0f, 1.0f,	-1.0f, -1.0f, 1.0f,	 1.0f,	-1.0f, -1.0f, 1.0f,

								  1.0f,	 -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,	1.0f,  1.0f,  1.0f,
								  1.0f,	 1.0f,	1.0f,  1.0f,  1.0f,	 -1.0f, 1.0f,  -1.0f, -1.0f,

								  -1.0f, -1.0f, 1.0f,  -1.0f, 1.0f,	 1.0f,	1.0f,  1.0f,  1.0f,
								  1.0f,	 1.0f,	1.0f,  1.0f,  -1.0f, 1.0f,	-1.0f, -1.0f, 1.0f,

								  -1.0f, 1.0f,	-1.0f, 1.0f,  1.0f,	 -1.0f, 1.0f,  1.0f,  1.0f,
								  1.0f,	 1.0f,	1.0f,  -1.0f, 1.0f,	 1.0f,	-1.0f, 1.0f,  -1.0f,

								  -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,	1.0f,  -1.0f, -1.0f,
								  1.0f,	 -1.0f, -1.0f, -1.0f, -1.0f, 1.0f,	1.0f,  -1.0f, 1.0f};
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *) 0);
	}

	void SkyBox::Render(Camera *camera)
	{
		glDepthFunc(GL_LEQUAL);
		sh.useShader();
		sh.setInt("skybox", 0);
		glm::mat4 view = glm::mat4(glm::mat3(camera->GetViewMatrix()));
		sh.setMat4("view", view);
		sh.setMat4("projection", camera->getProjectionMatrix());
		// skybox cube
		glBindVertexArray(VAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS);
	}
	void SkyBox::SetTextures(std::vector<std::string> paths) { textureID = FileIO::LoadCubeMapTextures(paths); }
	SkyBox::~SkyBox()
	{
		glDeleteBuffers(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteTextures(1, &textureID);
	}
}