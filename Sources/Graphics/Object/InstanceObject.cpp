#include "InstanceObject.hpp"
#include "glm/fwd.hpp"
#include <Math/Transformations.hpp>


#include <thread>
#include <algorithm>
namespace RenderEngine
{
	InstanceObject::InstanceObject(std::shared_ptr<RenderObject> object, unsigned int arraySize)
	{
		this->object = object;
		this->amount = arraySize;
		this->data	 = new ObjectData[arraySize];

		glGenBuffers(1, &buffer);
		
		modelMatrices = new glm::mat4[amount];



		SetBuffer();
		SetObject();
		first = false;
	}
	InstanceObject::InstanceObject(std::shared_ptr<RenderObject> o, ObjectData *data, unsigned int arraySize)
	{
		object = o;
		amount = arraySize;

		this->data = data;

		glGenBuffers(1, &buffer);

		modelMatrices = new glm::mat4[amount];



		SetBuffer();
		SetObject();

		first = false;
	}

	void InstanceObject::SetObject()
	{
		for (int i = 0; i < object->meshes.size(); i++) {
			unsigned int VAO = object->meshes[i].GetVAO();
			glBindVertexArray(VAO);
			glEnableVertexAttribArray(3);
			glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void *) 0);
			glEnableVertexAttribArray(4);
			glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void *) (sizeof(glm::vec4)));
			glEnableVertexAttribArray(5);
			glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
								  (void *) (2 * sizeof(glm::vec4)));
			glEnableVertexAttribArray(6);
			glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
								  (void *) (3 * sizeof(glm::vec4)));

			glVertexAttribDivisor(3, 1);
			glVertexAttribDivisor(4, 1);
			glVertexAttribDivisor(5, 1);
			glVertexAttribDivisor(6, 1);

			glBindVertexArray(0);
		}
	}

	void InstanceObject::UpdateData(ObjectData data, int pos)
	{
		this->data[pos] = data;
		auto matrix		= Transformations::getModelMatrix(data.position, data.rotation, data.scale);
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferSubData(GL_ARRAY_BUFFER, pos * sizeof(glm::mat4), sizeof(glm::mat4), &matrix);
	}
	ObjectData InstanceObject::GetData(int pos) { return data[pos]; }

	void InstanceObject::SetBuffer()
	{
		int start	  = 0;
		int end		  = amount / 2;
		std::thread t([&]() {
			for (unsigned int i = start; i < end; i++) {

				modelMatrices[i] =
					Transformations::getModelMatrix(data[i].position, data[i].rotation, data[i].scale);
			}
		});
		std::thread t2([&]() {
			for (unsigned int i = end; i < end*2; i++) {

				modelMatrices[i] =
					Transformations::getModelMatrix(data[i].position, data[i].rotation, data[i].scale);
			}
		});
		t.join();
		t2.join();
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		if(first){
			glBufferData(GL_ARRAY_BUFFER, amount * sizeof(glm::mat4), &modelMatrices[0], GL_STATIC_DRAW);
		}
		else
		{
			glBufferSubData(GL_ARRAY_BUFFER, 0 * sizeof(glm::mat4), amount * sizeof(glm::mat4),  &modelMatrices[0]);
		}

	}
	void InstanceObject::Render(ShaderProgram *shaderProgram)
	{
		if (object->meshes.size() > 0) {
			shaderProgram->useShader();
			if (GL_TEXTURE_2D, object->meshes[0].GetTexture(0) != nullptr) {
				shaderProgram->setInt("material.diffuse", 0);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, object->meshes[0].GetTexture(0)->id);
			}
			for (unsigned int i = 0; i < object->meshes.size(); i++) {
				glBindVertexArray(object->meshes[i].GetVAO());
				glDrawElementsInstanced(GL_TRIANGLES, object->meshes[i].GetIndices()->size(), GL_UNSIGNED_INT,
										0, amount);
				glBindVertexArray(0);
			}
		}
	}

}
