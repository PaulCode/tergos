#pragma once
#include <Graphics/Object/RenderObject.hpp>
#include <Graphics/ShaderProgram/ShaderProgram.hpp>
#include <vector>
#include <glm/glm.hpp>
#include <memory.h>

namespace RenderEngine
{

    /**
     * @brief Used to describe the position, rotation and scale of a  single object rendered in a Instance
     * 
     */
    struct ObjectData
    {
        glm::vec3 position = {0,1,2};
        glm::vec3 rotation = {0,0,0};
        glm::vec3 scale = {1,1,1};
        float lifeTime = 0;
    };


    /**
     * @brief Used when we need to render a single object more than once on the screen
     * \code{.cpp}
     * //example Usage
     *  InstanceObject* obj = new InstanceObject(renderObject, objectDataArray, sizeOfObjectDataArray);
     *  Engine::GetInstance()->AddInstancedObject(obj);
     * \endcode
     * 
     */
    class InstanceObject
    {
    public:

        /**
         * @brief Construct a new Instance Object object. Amount of rendered object is saved beforehand
         * 
         * @param object 
         * @param data 
         * @param arraySize 
         */
        InstanceObject(std::shared_ptr<RenderObject> object,ObjectData *data, unsigned int arraySize);
        InstanceObject(std::shared_ptr<RenderObject> object, unsigned int arraySize);

        /**
         * @brief Only used by the renderer
         * 
         * @param ShaderProgram 
         */
        void Render(ShaderProgram* ShaderProgram);


        /**
         * @brief update the position Data from a single object of the object
         * 
         * @param data 
         * @param pos 
         */
        void UpdateData(ObjectData data, int pos);
        ObjectData GetData(int pos);
        std::vector<ObjectData> *getAllObjects();



        ~InstanceObject(){
            delete data;
        }


        ObjectData* data;
        void SetObject();
        void SetBuffer();
    private:

        friend class ParticleObject;

        void SetVAO();
        std::shared_ptr<RenderObject> object;
        glm::mat4 *modelMatrices;
        unsigned int buffer;
        unsigned int amount;
    bool first = true;

    };

}
