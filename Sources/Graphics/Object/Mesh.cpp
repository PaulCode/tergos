#include "Mesh.hpp"
#include "Base/Renderer.hpp"
#include <iostream>
#include <algorithm>


namespace RenderEngine
{

	Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures)
	{
		this->vertices = vertices;
		this->indices  = indices;
		this->textures = textures;

		SetupMesh();
	}
	Mesh::Mesh(float *vertices, int size, unsigned int *indices, int indSize, float *textureCoords,
			   float *normals)
	{
		std::vector<unsigned int> ind;
		for (int i = 0; i < indSize; i++) {
			ind.push_back(indices[i]);
		}
		int texturePos = 0;
		std::vector<Vertex> vertexeVector;
		for (int i = 0; i < size; i += 3) {
			Vertex vert;
			glm::vec3 verti;
			glm::vec3 normal	   = {};
			glm::vec2 textureCoord = {};

			verti.x		  = vertices[i];
			verti.y		  = vertices[i + 1];
			verti.z		  = vertices[i + 2];
			vert.Position = verti;

			if (normals != nullptr) {
				normal.x = normals[i];
				normal.y = normals[i + 1];
				normal.z = normals[i + 2];
			}
			vert.Normal = normal;

			if (textureCoords != nullptr) {
				textureCoord.x = textureCoords[texturePos];
				textureCoord.y = textureCoords[texturePos++];
			}
			vert.TexCoords = textureCoord;

			vertexeVector.push_back(vert);
		}
		this->vertices = vertexeVector;
		this->indices  = ind;

		SetupMesh();
	}

	void Mesh::Render(ShaderProgram *shaderPr,unsigned int cubeMapTextureID)
	{

		shaderPr->setBool("material.useLight", material.useLight);
		shaderPr->setBool("material.useVertexColor", material.useVertexColour);
		shaderPr->setBool("material.useSolidColor",material.useSolidColor);
		shaderPr->setVec4("material.solidColor",material.solidColor);
		shaderPr->setFloat("material.shininess",material.shininess);
		shaderPr->setFloat("material.reflectionPercentage", material.reflectionPercentage);
		unsigned int diffuseNr	= 1;
		unsigned int specularNr = 1;
		unsigned int normalNr	= 1;
		unsigned int heightNr	= 1;
		unsigned int i = 0;
		for (i = 0; i < textures.size(); i++) {
			glActiveTexture(GL_TEXTURE0 + i);
			std::string name = textures[i].type;
			glUniform1i(glGetUniformLocation(shaderPr->getShaderProgram(), (name).c_str()), i);
			glBindTexture(GL_TEXTURE_2D, textures[i].id);
		}
		glActiveTexture(GL_TEXTURE0+i);
		std::string name = "skybox";
		glUniform1i(glGetUniformLocation(shaderPr->getShaderProgram(), (name).c_str()), i);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTextureID);


		//Set ID of texture here

		
		// draw mesh
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

		glActiveTexture(GL_TEXTURE0);
	}

	void Mesh::SetupMesh()
	{
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0],
					 GL_STATIC_DRAW);

		// vertex positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) 0);
		// vertex normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, Normal));
		// vertex texture coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, TexCoords));
		// vertexColour
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *) offsetof(Vertex, Color));

		glBindVertexArray(0);
	}

	void Mesh::AddTexture(Texture texture) { textures.push_back(texture); }

	void Mesh::AddTextures(std::vector<Texture> newTextures)
	{
		textures.insert(textures.end(), newTextures.begin(), newTextures.end());
	}

	unsigned int Mesh::GetVAO() { return VAO; }

	std::vector<unsigned int> *Mesh::GetIndices() { return &indices; }

	Texture *Mesh::GetTexture(int id) { return &textures[id]; }

	std::vector<Texture> &Mesh::GetTextures() { return textures; }

	Mesh::~Mesh() {}

	float Mesh::GetBoundingSphereRadius()
	{
		float radius = 0;
		for (auto &&vertex : vertices) {
			radius = std::max(radius, glm::length(vertex.Position));
		}
		return radius;
	}

	void Mesh::EnableLight(bool ligth){
		this->material.useLight = ligth;
	}
	void Mesh::EnableVertexColour(bool vert){
		this->material.useVertexColour = vert;
	}


	void Mesh::UseSolidColor(bool useColor)
	{
		this->material.useSolidColor = useColor;
	}
	bool Mesh::GetUseSolidColor(){
		return material.useSolidColor;
	}	

	void Mesh::SetSolidColor(glm::vec4 color){
		this->material.solidColor = color;
	}
	glm::vec4 Mesh::GetSolidColor(){
		return material.solidColor;
	}


	Material* Mesh::GetMaterial(){
		return &material;
	}


	void Mesh::SetMaterial(Material material){
		this->material = material;
	}

	void Mesh::CleanUp()
	{
		glDeleteVertexArrays(1, &VAO);
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
	}
} // namespace RenderEngine
