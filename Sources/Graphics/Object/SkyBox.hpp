#pragma once
#include <Graphics/ShaderProgram/ShaderProgram.hpp>
#include <Base/Camera.hpp>
#include <vector>
#include <string>
namespace RenderEngine
{
	class Renderer;
	class SkyBox
	{
	public:
		SkyBox() = default;
		void SetTextures(std::vector<std::string> paths);
		void Setup(std::vector<std::string> paths);

	private:
		~SkyBox();

		void Render(Camera *camera);
		unsigned int VAO, VBO, textureID;
		ShaderProgram sh;
		friend Renderer;
	};

}
