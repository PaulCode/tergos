#pragma once

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <vector>
#include <Graphics/ShaderProgram/ShaderProgram.hpp>

namespace RenderEngine
{
    class RenderObject;


	 struct Material {
		bool useLight = false;
		bool useVertexColour = false;
		bool useSolidColor = false;
		float shininess = 32.0f;
		glm::vec4 solidColor = {1,1,1,1};
		float reflectionPercentage = 0;
	 };

	/**
	 * @brief a Vertex struct is only used during mesh constructed
	 *
	 */

	struct Vertex {
		// position
		glm::vec3 Position;
		// normal
		glm::vec3 Normal;
		// texCoords
		glm::vec2 TexCoords;
		//colour
		glm::vec3 Color = glm::vec3(0,1,0);
		// tangent
		glm::vec3 Tangent; // currently not used
		// bitangent
		glm::vec3 Bitangent; // currently not used
	};

	/**
	 * @brief used to store information about textures
	 *
	 */
	struct Texture {
		unsigned int id;
		std::string type;
		std::string path;
	};

	/**
	 * @brief used to store a single mesh which can than be added to a renderObject.
	 *
	 */
	class Mesh
	{
	public:
		Mesh();
		~Mesh();

		/**
		 * @brief Construct a new Mesh, with texture
		 *
		 * @param vertices
		 * @param indices
		 * @param textures
		 */
		Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);

		/**
		 * @brief Construct a new Mesh. textures and normals not important
		 *
		 * @param vertices
		 * @param size
		 * @param indices
		 * @param indSize
		 * @param textureCoords
		 * @param normals
		 */
		Mesh(float *vertices, int size, unsigned int *indices, int indSize, float *textureCoords,
			 float *normals);

		/**
		 * @brief add a new Texture to the current mesh
		 *
		 * @param texture
		 */
		void AddTexture(Texture texture);

		/**
		 * @brief add new Textures to the current mesh
		 *
		 * @param texture
		 */
		void AddTextures(std::vector<Texture> textures);

		/**
		 * @brief Get the Texture by its id
		 *
		 * @param id
		 * @return Texture*
		 */
		Texture *GetTexture(int id);

		/**
		 * @return Reference to the internal textures vector
		 */
		std::vector<Texture> &GetTextures();

		/**
		 * @brief Get the Vertex Array object of the mesh
		 *
		 * @return unsigned int
		 */
		unsigned int GetVAO();

		/**
		 * @brief Get all Indices of a mesh
		 *
		 * @return std::vector<unsigned int>*
		 */
		std::vector<unsigned int> *GetIndices();

		float GetBoundingSphereRadius();

		void EnableLight(bool ligth);
		void EnableVertexColour(bool light);

		std::vector<Texture> textures;


		void UseSolidColor(bool useColor);
		bool GetUseSolidColor();

		void SetSolidColor(glm::vec4 color);
		glm::vec4 GetSolidColor();


		Material* GetMaterial();


		void SetMaterial(Material material);

	private:

	
		Material material;


		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		unsigned int VAO;
		unsigned int VBO, EBO;



		void SetupMesh();


        friend RenderObject;
		/**
		 * @brief called by the renderer when the object needs to be rendered. Can be ignored by the user
		 *
		 * @param spaderPr
		 */
		void Render(ShaderProgram *spaderPr,unsigned int cubeMapTextureID);

        void CleanUp();
	};

} // namespace RenderEngine
