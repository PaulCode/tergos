#pragma once

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <iostream>

namespace RenderEngine
{

	/**
	 * @brief Window is currently the only way to render the scene. It is automatically generated on startup
	 *
	 */
	class Window
	{
	public:
		~Window();

		int GetWidth();
		int GetHeigth();

		void SetWidth(int width);
		void SetHeight(int height);

		GLFWwindow *GetWindow();

		/**
		 * @brief initalizes the Window and the Opengl Context. Needs to be called before doing any other
		 * Openglcalls
		 *
		 * @param width
		 * @param height
		 * @param title
		 * @return int
		 */
		int Init(int width, int height, const char *title);

		void GrabMouse();

		/**
		 * @brief Get the only existing Instance of the Window. makes sure only one instance exists
		 *
		 * @return Window*
		 */
		static Window *GetInstance();

	private:
		bool m_ToggleMouse = false;

		static Window *instance;
		int width = 0, height = 0;
		bool debugMode = false;
		Window();
		GLFWwindow *windowHandle;
		/**
		 * @brief sets the size of used Window
		 *
		 * @param window
		 * @param width
		 * @param height
		 */
		static void framebuffer_size_callback(GLFWwindow *window, int width, int height);


	};

} // namespace RenderEngine
