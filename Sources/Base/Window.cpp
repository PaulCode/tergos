#include "Window.hpp"
#include <assimp/Importer.hpp>
#include <spdlog/spdlog.h>

RenderEngine::Window *RenderEngine::Window::instance = 0;

namespace RenderEngine
{
	Window::Window() {}

	Window::~Window() {}

	Window *Window::GetInstance()
	{
		if (!instance) {
			spdlog::debug("New Window created");
			instance = new Window;
		}
		return instance;
	}

	void Window::SetWidth(int w) { this->width = w; }

	GLFWwindow *Window::GetWindow() { return this->windowHandle; }

	void Window::GrabMouse() { glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED); }

	void Window::SetHeight(int h) { this->height = h; }

	int Window::GetHeigth()
	{
		return this->height;
		glViewport(0, 0, this->width, this->height);
	}

	int Window::GetWidth()
	{
		return this->width;
		glViewport(0, 0, this->width, this->height);
	}


    /**
     * @brief Initializes the Window
     * 
     * @param width 
     * @param height 
     * @param title 
     * @return int 
     */
	int Window::Init(int width, int height, const char *title)
	{
		SetWidth(width);
		SetHeight(height);

		if (!glfwInit())
			return -1;
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		windowHandle = glfwCreateWindow(width, height, title, NULL, NULL);
		if (!windowHandle) {
			glfwTerminate();
			return -1;
		}

		/* Make the window's context current */
		glfwMakeContextCurrent(windowHandle);
		glewExperimental = GL_TRUE;
		glfwSetFramebufferSizeCallback(windowHandle, framebuffer_size_callback);
		if (glewInit() != GLEW_OK)
			spdlog::critical("glew not init");

		glViewport(0, 0, width, height);

		return 0;
	}

	/**
	 * @brief called when window size needs to be updated
	 *
	 * @param w
	 * @param wi
	 * @param h
	 */
	void Window::framebuffer_size_callback(GLFWwindow *w, int wi, int h)
	{
		glViewport(0, 0, wi, h);
		Window::GetInstance()->SetHeight(h);
		Window::GetInstance()->SetWidth(wi);
	}
}