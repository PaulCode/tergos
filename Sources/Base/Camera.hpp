#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../Base/Window.hpp"

namespace RenderEngine
{
    /**
     * @brief The Camera is used to represent your position in the Worldspace. Only one camera can be used at a time but you can create multiple and change them using Engine::SetCamera()
     */
    class Camera
    {
    public:
        Camera();
        ~Camera();
        
        glm::vec3 GetPosition();
        glm::vec3 GetDirection();
        float GetFoV();
        /**
         * @brief Get the Vertical Angle of the Camera
         * 
         * @return float 
         */
        float GetVertAngle();

        /**
         * @brief Get the Horizontal Angle of the Camera
         * 
         * @return float 
         */
        float GetHorAngle();
        void SetPosition(glm::vec3 pos);
        /**
         * @brief Set the Horizontal Angle of the Camera
         * 
         * @param angle 
         */
        void SetHorizontalAngle(float angle);
        /**
         * @brief Set the Vertical Angle of the Camera
         * 
         * @param angle 
         */
        void SetVerticalAngle(float angle);
        void SetFoV(float fov);

        glm::mat4 GetViewMatrix();
        glm::mat4 getProjectionMatrix();
    private:
        glm::vec3 position;

        float horizontalAngle = 3.14f;
        float verticalAngle = 0.0f;
        float FoV = 45.0f;
    };
} // namespace RenderEngine
