#include "Camera.hpp"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace RenderEngine
{
    Camera::Camera()
    {
        position = glm::vec3(0, 0, 5);
    }

    Camera::~Camera()
    {
    }

    void Camera::SetHorizontalAngle(float angle){
        horizontalAngle = angle;
    }
    
    void Camera::SetVerticalAngle(float angle){
        verticalAngle = angle;
    }

    void Camera::SetFoV(float fov){
        FoV = fov;
    }

    float Camera::GetFoV()
    {
        return FoV;
    }

    void Camera::SetPosition(glm::vec3 pos){
        position.x = pos.x;
        position.y = pos.y;
        position.z = pos.z;
    }
    
    float Camera::GetVertAngle()
    {
        return verticalAngle;
    }
    
    float Camera::GetHorAngle()
    {
        return horizontalAngle;
    }

    glm::vec3 Camera::GetPosition()
    {
        return position;
    }

    glm::vec3 Camera::GetDirection() {
        float horizontalAngle = this->GetHorAngle();
        float verticalAngle = this->GetVertAngle();

        return glm::vec3(
            cos(verticalAngle) * sin(horizontalAngle),
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle)
        );
    }

    glm::mat4 Camera::GetViewMatrix() {
        glm::vec3 position = GetPosition();
        float horizontalAngle = GetHorAngle();
        float verticalAngle = GetVertAngle();

        glm::vec3 direction(
            cos(verticalAngle) * sin(horizontalAngle),
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle));

        glm::vec3 right = glm::vec3(
            sin(horizontalAngle - 3.14f / 2.0f),
            0,
            cos(horizontalAngle - 3.14f / 2.0f));

        glm::vec3 up = glm::cross(right, direction);

        glm::mat4 viewMatrix = glm::mat4(1);

        viewMatrix = glm::lookAt(
            position,
            position + direction,
            up);

        return viewMatrix;
    }





    glm::mat4 Camera::getProjectionMatrix()
    {
        glm::mat4 projectionMatrix = glm::mat4(1.0f);
        int width = Window::GetInstance()->GetWidth();
        int height = Window::GetInstance()->GetHeigth();
        projectionMatrix = glm::perspective(glm::radians(GetFoV()), (float)width / (float)height, 0.1f, 10000.0f);

        return projectionMatrix;
    }
}
