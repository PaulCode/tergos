#include "Renderer.hpp"

#include <string_view>

#include "Graphics/ShaderProgram/ShaderProgram.hpp"
#include "Graphics/imgui/imgui.h"
#include "Graphics/imgui/imgui_impl_glfw.h"
#include "Graphics/imgui/imgui_impl_opengl3.h"
#include <spdlog/spdlog.h>
#include "Utils/FileIO.hpp"

RenderEngine::Renderer *RenderEngine::Renderer::instance = 0;

namespace RenderEngine
{
	Renderer::Renderer() {}

	Renderer *Renderer::GetInstance()
	{
		if (!instance) {
			spdlog::debug("New Renderer created");
			instance = new Renderer();
		}
		return instance;
	}

	Renderer::~Renderer() {}

	bool Renderer::EnableLineRenderer(bool enable)
	{
		debugMode = enable;

		if (debugMode) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // this tells it to only render lines
		} else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

		return debugMode;
	}

	void Renderer::SetClearColour(float a, float g, float b, float f) { glClearColor(a, b, g, f); }

	void Renderer::Init()
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glEnable(GL_DEPTH_TEST);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glEnable(GL_MULTISAMPLE);

#ifdef ShowErrors

#endif
		glfwWindowHint(GLFW_SAMPLES, 16);
		glEnable(GL_MULTISAMPLE);

		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO &io = ImGui::GetIO();
		(void) io;
		// io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
		// io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

		// Setup Dear ImGui style
		ImGui::StyleColorsDark();
		// ImGui::StyleColorsClassic();

		// Setup Platform/Renderer backends
		ImGui_ImplGlfw_InitForOpenGL(Window::GetInstance()->GetWindow(), true);
		ImGui_ImplOpenGL3_Init("#version 130");


		glGenFramebuffers(1, &framebuffer);
		

		lightDebug = ShaderProgram("Resources/Shaders/lightdebugshader/light_debug.vs","Resources/Shaders/lightdebugshader/light_debug.fs");
		screenShader = ShaderProgram("Resources/Shaders/screenshader.vs","Resources/Shaders/screenshader.fs");
		blurShader = ShaderProgram("Resources/Shaders/bloom/blur.vs", "Resources/Shaders/bloom/blur.fs");

		blurShader.useShader();
		blurShader.setInt("image", 0);
	    float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
        // positions   // texCoords
        -1.0f,  1.0f,  0.0f, 1.0f,
        -1.0f, -1.0f,  0.0f, 0.0f,
         1.0f, -1.0f,  1.0f, 0.0f,

        -1.0f,  1.0f,  0.0f, 1.0f,
         1.0f, -1.0f,  1.0f, 0.0f,
         1.0f,  1.0f,  1.0f, 1.0f
    	};

		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

		glGenRenderbuffers(1, &depthBuffer);
		glGenFramebuffers(1, &framebuffer);
    	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    	glGenTextures(2, colorBuffers);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glGenFramebuffers(1, &pingpongFBO);
    	glGenTextures(1,&pingpongColorbuffers);


		pointLightTexture =  FileIO::GetTexture("Resources/Textures/debug_textures/point_light.png", "png");
		spotLightTexture =  FileIO::GetTexture("Resources/Textures/debug_textures/spot_light.png", "png");

		glGenTextures(1, &skyboxID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxID);


		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		for (unsigned int face = 0; face < 6; face++) {
        	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, GL_RGB, 250,
							 250, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    	}
	}

	void Renderer::Render(std::unordered_map<std::shared_ptr<ShaderHandler> , std::vector<std::shared_ptr<RenderObject>>> &renderObjects,
						  std::vector<std::shared_ptr<InstanceObject>> *instanceObjects, Light *light,
						  ShaderProgram *instaceShader,Texture* renderTarget, Camera *camera,
						  const PostProcessingPresets& post,
						  glm::vec2 textureSize,
						  bool calculateReflection)
	{
		Window* window = Window::GetInstance();


		if(textureSize.x < 0 || textureSize.y < 0)
		{
			textureSize.x = window->GetWidth();
			textureSize.y = window->GetHeigth();
		}


	




    	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);



		for (unsigned int i = 0; i < 2; i++)
    	{
			glBindTexture(GL_TEXTURE_2D, colorBuffers[i]);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, textureSize.x, textureSize.y, 0, GL_RGBA, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);  // we clamp to the edge as the blur filter would otherwise sample repeated texture values!
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			// attach texture to framebuffer
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorBuffers[i], 0);
    	}


		glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,textureSize.x, textureSize.y);
		unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    	glDrawBuffers(2, attachments);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        	std::cout << "Framebuffer not complete!" << std::endl;

		glBindRenderbuffer(GL_RENDERBUFFER, 0);





//		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderTarget->id, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER,depthBuffer);            
		glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);





		//Have to set ID of this here

		/*

			A ID needs to be added for this texture,
			 problem is that the ID needs to be gived depending on the exisiting textures of the object,

			 so the cubemap texture needs to be passed down to this object.

		*/

		for (const auto &mapPart : renderObjects) {
			if (mapPart.second.size() > 0) {
				mapPart.first->BindShader(light, camera);

				ShaderProgram *shaderProgram = mapPart.first->GetShaderProgram();
				shaderProgram->setBool("partymode", partyMode);
				shaderProgram->setVec3("threshold", treshold);
				for (auto it = mapPart.second.begin(); it != mapPart.second.end(); it++) {
					if (*it != nullptr && (*it)->GetVisibility()) {
						(*it)->Render(shaderProgram,skybox.textureID);
					}
				}
			}
		}

		if (instanceObjects->size() > 0) {
			instaceShader->useShader();
			instaceShader->setMat4("projection", camera->getProjectionMatrix());
			instaceShader->setMat4("view", camera->GetViewMatrix());

			for (auto it = instanceObjects->begin(); it != instanceObjects->end(); it++) {
				if (*it != nullptr) {
					(*it)->Render(instaceShader);
				}
			}
		}
		
		
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		if (useSkyBox)
			skybox.Render(camera);


		if(debugLight)
		{
        	glDisable(GL_DEPTH_TEST); 
			std::vector<RenderEngine::PointLight> vector = light->GetPointLights();
			lightDebug.useShader();
			lightDebug.setMat4("projection", camera->getProjectionMatrix());
			lightDebug.setMat4("view", camera->GetViewMatrix());
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, pointLightTexture.id);
			for (auto &vec : light->m_PointLights) {
				glm::mat4 model = Transformations::getModelMatrix(vec.position, camera->GetDirection(), {1,1,1});
				lightDebug.setMat4("model", model);
				glBindVertexArray(quadVAO);
				glDrawArrays(GL_TRIANGLES, 0, 6);	
			}

			glBindTexture(GL_TEXTURE_2D, spotLightTexture.id);
			for (auto &vec : light->m_SpotLights) {
				glm::mat4 model = Transformations::getModelMatrix(vec.position, {0,0,0}, {1,1,1});
				lightDebug.setMat4("model", model);
				glBindVertexArray(quadVAO);
				glDrawArrays(GL_TRIANGLES, 0, 6);	
			}
			glEnable(GL_DEPTH_TEST); 
		}


        glBindFramebuffer(GL_FRAMEBUFFER, 0);



		for (unsigned int i = 0; i < 1; i++)
		{

		}

		auto bloom = post.GetBloom();

		if(bloom.useBloom)
		{
			glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO);
			glBindTexture(GL_TEXTURE_2D, pingpongColorbuffers);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, textureSize.x, textureSize.y, 0, GL_RGBA, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingpongColorbuffers, 0);

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
				std::cout << "Framebuffer not complete!" << std::endl;

			blurShader.useShader();
			glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO);
			blurShader.setInt("horizontal", false);
			blurShader.setFloat("directions", bloom.directions);
			blurShader.setFloat("quality", bloom.quality);
			blurShader.setFloat("size", bloom.size);
			blurShader.setVec2("screenRes", textureSize);
			glBindTexture(GL_TEXTURE_2D, colorBuffers[1]); 
			RenderBlurQuad();
		}

		renderTarget->id = colorBuffers[0];
		first = false;
	}


	void Renderer::RenderBlurQuad(){
		if (blurQuadVAO == 0)
		{
			float quadVertices[] = {
				// positions        // texture Coords
				-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
				-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
				1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
				1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			};
			// setup plane VAO
			glGenVertexArrays(1, &blurQuadVAO);
			glGenBuffers(1, &blurQuadVBO);
			glBindVertexArray(blurQuadVAO);
			glBindBuffer(GL_ARRAY_BUFFER, blurQuadVAO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		}
		glBindVertexArray(blurQuadVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glBindVertexArray(0);
	}


	void Renderer::RenderToScreen(const Texture& renderTarget,PostProcessingPresets& postProcessingPresets)
	{
		if(debugMode)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // this tells it to only render lines
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST); // disable depth test so screen-space quad isn't discarded due to depth test.
        // clear all relevant buffers
        glClear(GL_COLOR_BUFFER_BIT);

        screenShader.useShader();
		screenShader.setInt("screenTexture", 0);
		screenShader.setInt("bloomTexture", 1);

		postProcessingPresets.SetShaderVariables(&this->screenShader);
        glBindVertexArray(quadVAO);
		glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, renderTarget.id);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, pingpongColorbuffers);
        glDrawArrays(GL_TRIANGLES, 0, 6);	
		
		if(debugMode)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // this tells it to only render lines
		}

	}
	void Renderer::RenderImGUI(Logic *logic)
	{
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
		logic->CreateUI();
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}

#pragma region SkyBox
	void Renderer::EnableSkyBox(bool enable) { useSkyBox = enable; }
	void Renderer::SetSkyBox(SkyBox skybox) {
		this->skybox = skybox;
	}
	void Renderer::SetupSkyBox(std::vector<std::string> faces )
	{
		skybox.Setup(faces);
	}
	SkyBox *Renderer::GetSkyBox() { return &skybox; }
#pragma endregion SkyBox
	void Renderer::OpenGlErrorMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
									  const GLchar *message, const void *userParam)
	{
		std::string_view _source;
		std::string_view _type;
		std::string_view _severity;

		switch (source) {
			case GL_DEBUG_SOURCE_API:
				_source = "API";
				break;

			case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				_source = "WINDOW SYSTEM";
				break;

			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				_source = "SHADER COMPILER";
				break;

			case GL_DEBUG_SOURCE_THIRD_PARTY:
				_source = "THIRD PARTY";
				break;

			case GL_DEBUG_SOURCE_APPLICATION:
				_source = "APPLICATION";
				break;

			case GL_DEBUG_SOURCE_OTHER:
				_source = "UNKNOWN";
				break;

			default:
				_source = "UNKNOWN";
				break;
		}

		switch (type) {
			case GL_DEBUG_TYPE_ERROR:
				_type = "ERROR";
				break;

			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				_type = "DEPRECATED BEHAVIOR";
				break;

			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				_type = "UDEFINED BEHAVIOR";
				break;

			case GL_DEBUG_TYPE_PORTABILITY:
				_type = "PORTABILITY";
				break;

			case GL_DEBUG_TYPE_PERFORMANCE:
				_type = "PERFORMANCE";
				break;

			case GL_DEBUG_TYPE_OTHER:
				_type = "OTHER";
				break;

			case GL_DEBUG_TYPE_MARKER:
				_type = "MARKER";
				break;

			default:
				_type = "UNKNOWN";
				break;
		}

		switch (severity) {
			case GL_DEBUG_SEVERITY_HIGH:
				_severity = "HIGH";
				break;

			case GL_DEBUG_SEVERITY_MEDIUM:
				_severity = "MEDIUM";
				break;

			case GL_DEBUG_SEVERITY_LOW:
				_severity = "LOW";
				break;

			case GL_DEBUG_SEVERITY_NOTIFICATION:
				_severity = "NOTIFICATION";
				break;

			default:
				_severity = "UNKNOWN";
				break;
		}

		if (_type == "ERROR") {
			spdlog::error("{}: {} of {} severity, raised from {}: {}", id, _type, _severity, _source, message);
		} else {
			spdlog::warn("{}: {} of {} severity, raised from {}: {}", id, _type, _severity, _source, message);
		}
	}

} // namespace RenderEngine
