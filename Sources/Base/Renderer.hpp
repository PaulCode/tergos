#pragma once
#include <Math/Transformations.hpp>
#include <Graphics/Light/Light.hpp>
#include <Graphics/ShaderProgram/ShaderProgram.hpp>
#include <Graphics/Object/RenderObject.hpp>
#include <Graphics/Object/InstanceObject.hpp>
#include <Graphics/PPP.hpp>
#include <Graphics/Object/SkyBox.hpp>
#include <Base/Camera.hpp>
#include <vector>
#include "Logic.hpp"
#include <unordered_map>
#include <Graphics/PPP.hpp>

#define ShowErrors

namespace RenderEngine
{

	/**
	 * @brief The 3D renderer of the Engine.
	 *
	 */
	class Renderer
	{
	public:
		~Renderer();

		/**
		 * @brief Called when we need to render the scene
		 *
		 * @param objects
		 * @param shaderPr
		 * @param camera
		 */
		void Render(std::unordered_map<std::shared_ptr<ShaderHandler> ,
		 std::vector<std::shared_ptr<RenderObject>>> &renderObject,
					std::vector<std::shared_ptr<InstanceObject>> *instanceObjects,
					Light *light,
				    ShaderProgram *instaceShader,
					Texture* RenderTarget,
					Camera *camera,
					const PostProcessingPresets& post,
					glm::vec2 textureSize = {-1,-1},
					bool calculateReflection = false);

		
		void RenderToScreen(const Texture& renderTarget,PostProcessingPresets& postProcessingPresets);
		/**
		 * @brief used by the Engine when IMGUI is enabled
		 *
		 * @param logic
		 */
		void RenderImGUI(Logic *logic);
		/**
		 * @brief Initialize the Renderer
		 *
		 */
		void Init();
		/**
		 * @brief changes between normal and line renderer
		 *
		 * @return true
		 * @return false
		 */
		bool EnableLineRenderer(bool enable);

		/**
		 * @brief Set the Clear Colour of the usedWindow
		 *
		 * @param a
		 * @param g
		 * @param b
		 * @param f
		 */
		void SetClearColour(float a, float g, float b, float f);

		/**
		 * @brief call this function to toggle the skybox;
		 *
		 */
		void EnableSkyBox(bool enable);
		static Renderer *GetInstance();
		/**
		 * @brief Setup the base Skybox. Don't use for your own skybox
		 *
		 */
		void SetupSkyBox(std::vector<std::string> faces = {
							 "Resources/Textures/skybox/right.jpg", "Resources/Textures/skybox/left.jpg",
							 "Resources/Textures/skybox/top.jpg", "Resources/Textures/skybox/bottom.jpg",
							 "Resources/Textures/skybox/front.jpg", "Resources/Textures/skybox/back.jpg"});
		SkyBox *GetSkyBox();
		void SetSkyBox(SkyBox skybox);


		glm::vec3 GetThreshold()
		{
			return treshold;
		}

		void SetThreshold(glm::vec3 tr)
		{
			treshold = tr;
		}

		void SetDebugLight(bool light)
		{
			debugLight = light;
		}

		bool GetDebugLight()
		{
			return debugLight;
		}


	bool partyMode = false; 

	private:
		static Renderer *instance;
		bool debugMode = false;
		bool useSkyBox = false;
		bool debugLight = false;
		unsigned int framebuffer;
		unsigned int depthBuffer;
	    unsigned int textureColorbuffer;

    	unsigned int colorBuffers[2];

		unsigned int pingpongFBO;
    	unsigned int pingpongColorbuffers;

		bool useBlur = true;

		glm::vec3 treshold = {0.2126, 0.7152, 0.0722};


		Texture pointLightTexture;
		Texture spotLightTexture;

		unsigned int quadVAO, quadVBO;
		unsigned int blurQuadVAO = 0;
		unsigned int blurQuadVBO;



		unsigned int skyboxID = 0;

		ShaderProgram screenShader;
		ShaderProgram blurShader;
		ShaderProgram lightDebug;
		

		bool first = true;
		SkyBox skybox;
		Renderer();
		void RenderBlurQuad();
		static void OpenGlErrorMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
									   const GLchar *message, const void *userParam);
	};

} // namespace RenderEngine
