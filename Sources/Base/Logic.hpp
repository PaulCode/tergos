#pragma once

namespace RenderEngine {

	
	/**
	 * @brief Logic Interface used to interact with the render engine
	 * 
	 */
	class Logic
	{
	public:
		/**
		 * @brief Init Method, called at startup
		 * 
		 */
		 virtual void Init(){};


		/**
		 * @brief write your imgui code inside of this function
		 * 
		 */
		virtual void CreateUI(){};	
		/**
		 * @brief called every frame. used to update the scene
		 * 
		 * @param deltaTime represents the time since last frame in seconds
		 */
		 virtual void Update(float deltaTime){};
	};
}