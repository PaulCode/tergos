md Build
cd Build

cmake --version

cmake -G "Visual Studio 16 2019" -T host=x64 -A x64 ..

cmake --build . --config Release --target ALL_BUILD

exit $LASTEXITCODE